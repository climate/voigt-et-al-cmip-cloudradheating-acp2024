2B-FLXHR-LIDAR data was processed by Aiko Voigt for release R05 and R04, see https://gitlab.phaidra.org/climate/cloudsat-calipso-heating-rates

----------------------------------------------------------------------------------------------------------

voigtetal2019_cloudsatcalipso_acre-zontimmean.nc contains the CRH data used in Voigt et al., 2019, J. Climate, https://journals.ametsoc.org/view/journals/clim/32/10/JCLI-D-18-0810.1.xml. However, this data is deprecated - instead the 2B-FLXHR-LIDAR datasets should be used.

----------------------------------------------------------------------------------------------------------

Information by email from Seung-Hee Ham on the CCCM CRH dataset (email from June 23, 2023):

Dear Aiko,

I sent an invitation to access the folder I placed the gridded dataset.
If you have not received the email, please let me know.

In the folder, the file "Y2007-2010_cim_CRE_on_HR.nc" includes cloud radiative effects on SW and LW heating rate profiles. 
The heating rate profiles for all sky and clear sky are from CCCM RelD level product, available at https://ceres-tool.larc.nasa.gov/ord-tool/products?CERESProducts=CCCM . 
SW heating rates are scaled by monthly gridded solar incoming flux using CERES SYN product.

I would like to let you know that the RelD CCCM product includes a bug that sometimes cloud base heights of deep convective clouds are overestimated since some of the CloudSat clouds were not included. This issue will be addressed in the future version, but no specific timeline is planned for future processing. Since the total column optical depth is constrained by MODIS, the issue should not affect the total cloud optical depth. 

Please also note that daytime LW heating rates are more accurate than nighttime LW heating rates. This is in part because nighttime cloud optical depth tends to be underestimated due to the limitation of IR retrievals. 

If you need any further information or have any problems with opening the file, please let me know.
If you can provide any feedback about the product, that would be great for future improvement as well. 

Best,
Seung-Hee Ham