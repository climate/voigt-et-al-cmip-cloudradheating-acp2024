#!/usr/bin/env python
# coding: utf-8
# # Download CMIP6 data from Metagrid in batch mode: tropopause pressure from AERmon table, only consider models for which CRH can be calculated

# on srvx1 call as
# /home/swd/manual/nwp/2023.1/bin/python3.10 cmip6-metagrid_batchdownload_ptp_AERmon.py

# Import lists for experiment, table and variable.
import lists as lists
import subprocess
import os

# Generate wget scripts.
base = "http://esgf-data.dkrz.de/esg-search/wget?download_structure=project,experiment_id,variable,table_id&project=CMIP6&latest=true&replica=true"

# list of models for which CRH can be calculated, taken from ../plots4paper/helpers.py
import sys
sys.path.append("../plots4paper/")
from helpers import models_amip, models_amipp4K, models_amipfuture4K

tab="AERmon"
var="ptp"

for exp in ["amip", "amip-p4K", "amip-future4K"]:
    if exp == "amip":
        for mod in models_amip:
            request = base + "&experiment_id=" + exp + "&table_id=" + tab + "&variable=" + var + "&source_id=" + mod
            script="/mnt/users/staff/avoigt/cmip-acre/data-download/wget.sh-"+exp+"-"+tab+"-"+var+"-"+mod
            subprocess.run(["wget", "--quiet", "-O", script, request])
    if exp == "amip-p4K":
        for mod in models_amipp4K:
            request = base + "&experiment_id=" + exp + "&table_id=" + tab + "&variable=" + var + "&source_id=" + mod
            script="/mnt/users/staff/avoigt/cmip-acre/data-download/wget.sh-"+exp+"-"+tab+"-"+var+"-"+mod
            subprocess.run(["wget", "--quiet", "-O", script, request])
    if exp == "amip-future4K":
        for mod in models_amipfuture4K:
            request = base + "&experiment_id=" + exp + "&table_id=" + tab + "&variable=" + var + "&source_id=" + mod
            script="/mnt/users/staff/avoigt/cmip-acre/data-download/wget.sh-"+exp+"-"+tab+"-"+var+"-"+mod
            subprocess.run(["wget", "--quiet", "-O", script, request])

# Execute wget scripts to retrieve data. Make sure to temporarily move to scratch disk for data download.
def valid_wget_script(script):
    """ Tests if wget bash script is valid, i.e., does not contain "No files were found that matched the query" """
    valid=True
    file = open(script, "r")
    if file.read() == "No files were found that matched the query":
        valid=False
    file.close()
    return valid

os.chdir("/scratch/das/avoigt/cmip6-acre-data/")
for exp in ["amip", "amip-p4K", "amip-future4K"]:
    if exp == "amip":
        for mod in models_amip:
            script="/mnt/users/staff/avoigt/cmip-acre/data-download/wget.sh-"+exp+"-"+tab+"-"+var+"-"+mod
            if valid_wget_script(script):
                fname="/mnt/users/staff/avoigt/cmip-acre/data-download/wget.sh-"+exp+"-"+tab+"-"+var+"-"+mod+".log"
                logfile = open(fname, "w")
                subprocess.Popen(["bash", script, "-s"], stdout=logfile)
                logfile.flush()
                logfile.close()
    if exp == "amip-p4K":
        for mod in models_amipp4K:
            script="/mnt/users/staff/avoigt/cmip-acre/data-download/wget.sh-"+exp+"-"+tab+"-"+var+"-"+mod
            if valid_wget_script(script):
                fname="/mnt/users/staff/avoigt/cmip-acre/data-download/wget.sh-"+exp+"-"+tab+"-"+var+"-"+mod+".log"
                logfile = open(fname, "w")
                subprocess.Popen(["bash", script, "-s"], stdout=logfile)
                logfile.flush()
                logfile.close()
    if exp == "amip-future4K":
        for mod in models_amipfuture4K:
            script="/mnt/users/staff/avoigt/cmip-acre/data-download/wget.sh-"+exp+"-"+tab+"-"+var+"-"+mod
            if valid_wget_script(script):
                fname="/mnt/users/staff/avoigt/cmip-acre/data-download/wget.sh-"+exp+"-"+tab+"-"+var+"-"+mod+".log"
                logfile = open(fname, "w")
                subprocess.Popen(["bash", script, "-s"], stdout=logfile)
                logfile.flush()
                logfile.close()
os.chdir("/mnt/users/staff/avoigt/cmip-acre/data-download/")
