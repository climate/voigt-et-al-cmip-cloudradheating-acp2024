#!/usr/bin/env python
# coding: utf-8
# Download CMIP6 data from Metagrid in batch mode: geopotential height zg from Amon table for HadGEM3 and UKESM
# Limit to the models for which CRH can be calculated

# on srvx1 call as
# /home/swd/manual/nwp/2023.1/bin/python3.10 cmip6-metagrid_batchdownload_zg_Amon_HadGEM-UKESM.py

# Import lists for experiment, table and variable.
import lists as lists
import subprocess
import os

scriptdir="/users/staff/avoigt/cmip6-crh/data-download/"
datadir="/scratch/das/avoigt/cmip6-acre-data/"

# Generate wget scripts.
base = "http://esgf-data.dkrz.de/esg-search/wget?download_structure=project,experiment_id,variable,table_id&project=CMIP6&latest=true&replica=true"

tab="Amon"
var="zg"

models_amip=["HadGEM3-GC31-LL", "HadGEM3-GC31-MM", "UKESM1-0-LL"]
models_amipp4K=["HadGEM3-GC31-LL"]
models_amipfuture4K=["HadGEM3-GC31-LL"]

for exp in ["amip", "amip-p4K", "amip-future4K"]:
    if exp == "amip":
        for mod in models_amip:
            request = base + "&experiment_id=" + exp + "&table_id=" + tab + "&variable=" + var + "&source_id=" + mod
            script=scriptdir+"wget.sh-"+exp+"-"+tab+"-"+var+"-"+mod
            print(["wget", "--quiet", "-O", script, request])
            subprocess.run(["wget", "--quiet", "-O", script, request])
    if exp == "amip-p4K":
        for mod in models_amipp4K:
            request = base + "&experiment_id=" + exp + "&table_id=" + tab + "&variable=" + var + "&source_id=" + mod
            script=scriptdir+"wget.sh-"+exp+"-"+tab+"-"+var+"-"+mod
            subprocess.run(["wget", "--quiet", "-O", script, request])
    if exp == "amip-future4K":
        for mod in models_amipfuture4K:
            request = base + "&experiment_id=" + exp + "&table_id=" + tab + "&variable=" + var + "&source_id=" + mod
            script=scriptdir+"wget.sh-"+exp+"-"+tab+"-"+var+"-"+mod
            subprocess.run(["wget", "--quiet", "-O", script, request])

# Execute wget scripts to retrieve data. Make sure to temporarily move to scratch disk for data download.
def valid_wget_script(script):
    """ Tests if wget bash script is valid, i.e., does not contain "No files were found that matched the query" """
    valid=True
    file = open(script, "r")
    if file.read() == "No files were found that matched the query":
        valid=False
    file.close()
    return valid

os.chdir(datadir)
for exp in ["amip", "amip-p4K", "amip-future4K"]:
    if exp == "amip":
        for mod in models_amip:
            script=scriptdir+"wget.sh-"+exp+"-"+tab+"-"+var+"-"+mod
            if valid_wget_script(script):
                fname=scriptdir+"wget.sh-"+exp+"-"+tab+"-"+var+"-"+mod+".log"
                logfile = open(fname, "w")
                subprocess.Popen(["bash", script, "-s"], stdout=logfile)
                logfile.flush()
                logfile.close()
    if exp == "amip-p4K":
        for mod in models_amipp4K:
            script=scriptdir+"wget.sh-"+exp+"-"+tab+"-"+var+"-"+mod
            if valid_wget_script(script):
                fname=scriptdir+"wget.sh-"+exp+"-"+tab+"-"+var+"-"+mod+".log"
                logfile = open(fname, "w")
                subprocess.Popen(["bash", script, "-s"], stdout=logfile)
                logfile.flush()
                logfile.close()
    if exp == "amip-future4K":
        for mod in models_amipfuture4K:
            script=scriptdir+"wget.sh-"+exp+"-"+tab+"-"+var+"-"+mod
            if valid_wget_script(script):
                fname=scriptdir+"wget.sh-"+exp+"-"+tab+"-"+var+"-"+mod+".log"
                logfile = open(fname, "w")
                subprocess.Popen(["bash", script, "-s"], stdout=logfile)
                logfile.flush()
                logfile.close()
os.chdir(datadir)
