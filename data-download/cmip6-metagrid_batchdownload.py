#!/usr/bin/env python
# coding: utf-8
# # Download CMIP6 data from Metagrid in batch mode: many experiments, tables and variables

# Import lists for experiment, table and variable.
import lists as lists
import subprocess
import os

# which variables do we download, from which list and which experiments
varlist = lists.list_cl
#tablist=lists.list_tb
tablist = ["Amon"] # cl is in Amon
explist = lists.list_ex

# Generate wget scripts.
#base = "http://esgf-data.dkrz.de/esg-search/wget?download_structure=project,experiment_id,variable,table_id&project=CMIP6&latest=true&replica=false"
base = "http://esgf-data.dkrz.de/esg-search/wget?download_structure=project,experiment_id,variable,table_id&project=CMIP6&latest=true&replica=true"

for var in varlist:
    for exp in explist:
        for tab in tablist:
            request = base + "&experiment_id=" + exp + "&table_id=" + tab + "&variable=" + var
            script="/users/staff/avoigt/cmip6-crh/data-download/wget.sh-"+exp+"-"+tab+"-"+var
            subprocess.run(["wget", "--quiet", "-O", script, request])

# Execute wget scripts to retrieve data. Make sure to temporarily move to scratch disk for data download.
def valid_wget_script(script):
    """ Tests if wget bash script is valid, i.e., does not contain "No files were found that matched the query" """
    valid=True
    file = open(script, "r")
    if file.read() == "No files were found that matched the query":
        valid=False
    file.close()
    return valid

os.chdir("/scratch/das/avoigt/cmip6-acre-data/")
for var in varlist:
    for exp in explist:
        for tab in tablist:
            script="/users/staff/avoigt/cmip6-crh/data-download/wget.sh-"+exp+"-"+tab+"-"+var
            if valid_wget_script(script):
                fname="/users/staff/avoigt/cmip6-crh/data-download/wget.sh-"+exp+"-"+tab+"-"+var+".log"
                logfile = open(fname, "w")
                subprocess.Popen(["bash", script, "-s"], stdout=logfile)
                logfile.flush()
                logfile.close()
os.chdir("/users/staff/avoigt/cmip6-crh/data-download/")
