#!/usr/bin/env python
# coding: utf-8

# Plots of clear-sky radiative heating (csrh) for all available models and the amip, amip-p4K and amip-future4K simulations
# Generates the following types of multi-panel plots of csrh:
#   * Plot 1: one panel per model, using only one csrh approach per model;
#   * Plot 2: one row per model and three columns: i) csrh from radiative fluxes provided by the CFmon table, ii) csrh from tntr provided by the CFmon, AERmon and Emon tables, and iii) csrh from tntr provided by the EmonZ table.
#   * Plot 3: vertical profiles of csrh averaged over 5 domains

# on srvx1 of img univie, call as follows:
# /home/swd/manual/nwp/2023.1/bin/python3.10 make_plots_csrh.py

import os
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt 
import matplotlib as mpl
import helpers as hlp

import warnings
warnings.simplefilter("ignore") 

#----------------------------------------------------------------------
# Defines switches re which plots should be generated
#----------------------------------------------------------------------
lplot1=True
lplot2=True
lplot3=True

#----------------------------------------------------------------------
# Defines plotting functions
#----------------------------------------------------------------------

# Plot 1:
# zonal-mean time-mean csrh for all available models of a given simulation in a 4-column panel plot
# and with tropopause and T=0 and T=-38deg C as lines
def plot_csrh_xypanelplot(exp, models):
    
    csrh_clev=np.delete(2.0*np.linspace(-1,1,21),10) # contour levels for csrh, remove 0
    nmod = len(models) # number of models
    
    ncol=4
    # number of panel rows as a function of number of models, we add one extra row to place colorbar there
    nrow=int(np.floor(nmod/ncol)) + 1
    if np.mod(nmod,ncol) > 0: nrow+=1

    def _plot_csrh_singledata(csrh, ax):
        """ Plots zonal-mean time-mean csrh. Works for both pressure-level and height-level data."""
        lat=csrh.lat
        if "lev" in csrh.coords: # pressure levels (almost all models)
            lev = csrh.lev.values
        elif "hgt" in csrh.coords: # height levels (HadGEM and UKESM models)
            csrh, lev = hlp.height2pressure_zg(csrh,mod,exp)
        cnt=plt.contourf(lat, lev/100, csrh, csrh_clev, cmap="RdBu_r", extend="both")
        for c in cnt.collections: c.set_edgecolor("face")
        plt.ylim(1000,10)
        plt.yticks([800,600,400,200],[" ", " ", " ", " "], size=12);
        plt.xlim(-88,88)
        plt.xticks([-60,-30,0,30,60],[" ", " ", " ", " ", " "], size=12, va="top");
        ax.tick_params(direction="in", length=6)
        return
   
    def _plot_ta_tp(ta, tp, ax):
        """ Plots temperature contours and tropopause, which are both on pressure levels."""
        plt.contour(ta.lat, ta.lev/100, ta, levels=[273.15-38, 273.15], colors="darkgray", linestyles="-", linewidths=0.5)
        plt.plot(tp.lat, tp, color="seagreen", linewidth=0.5)
        return
    
    # we use one extra row to place the colorbar there
    fig=plt.figure(figsize=(6*4,nrow*4))
    #fig, axs = plt.subplots(ncol, nrow, layout="constrained", figsize=(6*ncol,4*nrow))
        
    # loop over models, use csrh in following order: 1) from CFmon flux, 2) from tntr of CFmon, AERmon, Emon, 3) from tntr of EmonZ
    counter=0
    for mod in models:
        counter+=1
        # air temperature and tropopause
        ta = hlp.ta_Amon_ymonmean(mod, exp)["ta"].mean(["month", "lon"]).squeeze()
        tp = hlp.tropo_Amon_zontimmean(mod, exp)["trop"].squeeze()
        # csrh
        csrh = hlp.load_csrh_zontimmean(mod, exp)
        if mod in ["CESM2", "CESM2-FV2", "CESM2-WACCM", "CESM2-WACCM-FV2"]:
            if ta is not None: ta = ta.where(ta.lev<500e2) 
        # plot
        ax=plt.subplot(nrow, ncol, counter)
        _plot_csrh_singledata(csrh, ax)
        _plot_ta_tp(ta, tp, ax)
        if np.mod(counter, ncol) == 1:
            plt.ylabel("pressure / hPa", loc="top", fontsize=15)
            plt.yticks([800,600,400,200],["800", "600","400","200"], size=12);
        if counter>ncol*(nrow-2):
            plt.xlabel("latitude / deg", loc="right", fontsize=15)
            plt.xticks([-60,-30,0,30,60],["60S", "30S","Eq","30N","60N"], size=12, va="top");
        # for rows that are not completely filled, we need to include
        # xlabel in the panels of the row above
        if counter+ncol>nmod: 
            plt.xlabel("latitude / deg", loc="right", fontsize=15)
            plt.xticks([-60,-30,0,30,60],["60S", "30S","Eq","30N","60N"], size=12, va="top");
        ax.text(0.03,0.96, mod, ha="left", va="top", transform=ax.transAxes, backgroundcolor="white", 
                size=15, bbox=dict(facecolor="white", edgecolor="k", boxstyle="round,pad=0.3"))       

    # plot colorbar
    # solution taken and adapted from https://stackoverflow.com/a/62436015
    # axes position found manually, to do so use "print(ax)" for above subplots
    if exp=="amip-p4K" or exp=="amip-future4K":
        ax = fig.add_axes([0.714,0.381765,0.185, 0.04/nrow])
    if exp=="amip":
        ax = fig.add_axes([0.714,0.20,0.185, 0.04/nrow])
    cb = mpl.colorbar.ColorbarBase(ax, orientation="horizontal", 
                                   cmap="RdBu_r", extend="both", ticks=[-2, -1.4, -0.8, 0.0, 0.8, 1.4, 2.0],
                                   label="K/day",
                                   boundaries=csrh_clev, norm=mpl.colors.Normalize(csrh_clev[0], csrh_clev[-1]))     
    cb.ax.set_xticklabels(["-2.0", "-1.4", "-0.8", "0", "0.8", "1.4", "2.0"])
    cb.ax.tick_params(labelsize=15)
    cb.set_label(label="K/day", size=15)

    hlp.spacing_subplots()


# Plot 2:
# zonal-mean time-mean csrh for all available models and all available approaches as a 3-column panel plot
# with one row per model
def plot_csrh_3approaches(exp, models):

    csrh_clev=np.delete(2.0*np.linspace(-1,1,21),10) # contour levels for csrh, remove 0
    nmod = len(models) # number of models

    def _plot_csrh_singledata(csrh, ax):
        """ Plots zonal-mean time-mean csrh. Works for both pressure-level and height-level data."""
        # pressure levels (almost all models)
        if "lev" in csrh.coords:
            cnt=plt.contourf(csrh.lat, csrh.lev/100, csrh, csrh_clev, cmap="RdBu_r", extend="both")
            for c in cnt.collections: c.set_edgecolor("face")
            plt.ylim(1000,10)
            plt.yticks([800,600,400,200],[" ", " "," "," "], size=6);
        # height levels (HadGEM and UKESM models)
        elif "hgt" in csrh.coords:
            cnt=plt.contourf(csrh.lat, csrh.hgt/1000, csrh, csrh_clev, cmap="RdBu_r", extend="both")
            for c in cnt.collections: c.set_edgecolor("face")
            plt.ylim(0,20)
            plt.yticks([4,8,12,16],[" ", " ", " ", " "], size=6);
        plt.xlim(-88,88)
        plt.xticks([-60,-30,0,30,60],["60S", "30S","Eq","30N","60N"], size=6, va="top");
        ax.tick_params(direction="in", length=6)
        return

    fig=plt.figure(figsize=(10,nmod*1.5))

    # three top subplots for title of columns

    ax=plt.subplot(nmod+1, 3, 1); ax.set_axis_off()
    plt.text(0.5, 0.2, "From fluxes", fontsize=10, fontweight="bold", ha="center" , va="center", transform=ax.transAxes)

    ax=plt.subplot(nmod+1, 3, 2); ax.set_axis_off()
    plt.text(0.5, 0.2, "From tntr of CFmon, AERmon, Emon", fontsize=10, fontweight="bold", ha="center" , va="center", transform=ax.transAxes)

    ax=plt.subplot(nmod+1, 3, 3); ax.set_axis_off()
    plt.text(0.5, 0.2, "From tntr of EmonZ", fontsize=10, fontweight="bold", ha="center" , va="center", transform=ax.transAxes)

    counter=4 # counter for subplots
    plot_modname=False # True if model name was already added
    for mod in models:
        # from fluxes
        csrh = hlp.csrh_from_flx_ymonmean(mod, exp)
        if csrh is not None:
            ax=plt.subplot(nmod+1, 3, counter); _plot_csrh_singledata(csrh.mean(["month", "lon"]), ax)
            plt.text(-0.2, 0.5, mod, fontsize=8, fontweight="bold", ha="center" , va="center", rotation=90, transform=ax.transAxes)
            # pressure levels (almost all models)
            if "lev" in csrh.coords:
                plt.yticks([800,600,400,200],["800", "600","400","200"], size=6);
                plt.ylabel("pressure / hPa", loc="top", fontsize=6)
            # height levels (HadGEM and UKESM models)
            elif "hgt" in csrh.coords:
                plt.yticks([4,8,12,16],["4", "8", "12", "16"], size=6);
                plt.ylabel("altitude / km", loc="top", fontsize=6)
            plot_modname=True
        # from tntr via CFmon, AERmon, Emon
        csrh = hlp.csrh_from_tntr_ymonmean(mod, exp)
        if csrh is not None:
            ax=plt.subplot(nmod+1, 3, counter+1); _plot_csrh_singledata(csrh.mean(["month", "lon"]), ax)
            if plot_modname==False:
                plt.text(-1.281, 0.5, mod, fontsize=8, fontweight="bold", ha="center" , va="center", rotation=90, transform=ax.transAxes)
                # pressure levels (almost all models)
                if "lev" in csrh.coords:
                    plt.yticks([800,600,400,200],["800", "600","400","200"], size=6);
                    plt.ylabel("pressure / hPa", loc="top", fontsize=6)
                # height levels (HadGEM and UKESM models)
                elif "hgt" in csrh.coords:
                    plt.yticks([4,8,12,16],["4", "8", "12", "16"], size=6);
                    plt.ylabel("altitude / km", loc="top", fontsize=6)
                plot_modname=True
        # from tntr via EmonZ
        csrh = hlp.csrh_from_tntr_EmonZ_ymonmean(mod, exp)
        if csrh is not None:
            ax=plt.subplot(nmod+1, 3, counter+2);
            # for CESM models, need to set spuriously large values for csrh to zero
            if mod in  ["CESM2", "CESM2-FV2", "CESM2-WACCM", "CESM2-WACCM-FV2"]:
                aux=csrh.mean(["month"])
                aux = xr.where(aux>2.0,0,aux)     
                _plot_csrh_singledata(aux, ax)
            else:
                _plot_csrh_singledata(csrh.mean(["month"]), ax)
            if plot_modname==False:
                plt.text(-2.359, 0.5, mod, fontsize=8, fontweight="bold", ha="center" , va="center", rotation=90, transform=ax.transAxes)
                # pressure levels (almost all models)
                if "lev" in csrh.coords:
                    plt.yticks([800,600,400,200],["800", "600","400","200"], size=6);
                    plt.ylabel("pressure / hPa", loc="top", fontsize=6)
                # height levels (HadGEM and UKESM models)
                elif "hgt" in csrh.coords:
                    plt.yticks([4,8,12,16],["4", "8", "12", "16"], size=6);
                    plt.ylabel("altitude / km", loc="top", fontsize=6)
        # move on to next model --> increase subplot counter by 3
        counter=counter+3
        plot_modname=False

    # plot colorbar
    # solution taken and adapted from https://stackoverflow.com/a/62436015
    # axes position defined manually as a function of experiment
    if exp=="amip-p4K" or exp=="amip-future4K":
        ax = fig.add_axes([0.3, 0.05, 0.4, 0.06/nmod])
    if exp=="amip":
        ax = fig.add_axes([0.3, 0.08, 0.4, 0.06/nmod])
    cb = mpl.colorbar.ColorbarBase(ax, orientation="horizontal", 
                                   cmap="RdBu_r", extend="both", ticks=[-2, -1.4, -0.8, 0.0, 0.8, 1.4, 2.0],
                                   label="K/day",
                                   boundaries=csrh_clev, norm=mpl.colors.Normalize(csrh_clev[0], csrh_clev[-1]))  
    cb.ax.set_xticklabels(["-2.0", "-1.4", "-0.8", "0", "0.8", "1.4", "2.0"])
    cb.ax.tick_params(labelsize=8)
    cb.set_label(label="K/day", size=8)

    hlp.spacing_subplots_v2()


# Plot 3:
# 5 panel plot for 5 domain averages ordered from South to North
def plot_csrh_5domains(exp, models):

    nmod = len(models) # number of models

    def _make_niceaxes(ax, ticklength=0.08):
        # adjust spines
        ax.spines["top"].set_color("none")
        ax.spines["right"].set_color("none")
        ax.xaxis.set_ticks_position("bottom")
        ax.spines["bottom"].set_position(("data",1000))
        ax.spines["left"].set_position(("data",-1.5))
        ax.spines["left"].set_color("none")
        plt.ylim(1000,10)
        plt.yticks([ ], fontsize=10) 
        plt.plot([0,0], [1000,10], linewidth=0.7, color="k", zorder=-10)
        for ypos in [1000,800,600,400,200,10]:
            plt.plot([-0.5*ticklength,0.5*ticklength], [ypos,ypos], linewidth=0.7, color="k", zorder=-10)

    def _plot_csrh_singledata(_csrh, mod):
        """ Plots domain-mean time-mean csrh. Assumes pressure levels."""
        plt.plot(_csrh, _csrh.lev/100, color="gray", label=mod)
        plt.yticks([800,600,400,200],["800", "600","400","200"], size=6);
        return

    def _height2pressurelevels(_csrh, mod, exp):
        """ Interpolates from height to common pressure levels for HadGEM and UKESM models."""
        if mod in ["HadGEM3-GC31-LL", "HadGEM3-GC31-MM", "UKESM1-0-LL"]:
            _csrh_int, lev = hlp.height2pressure_zg(_csrh, mod, exp)
            # _csrh_int is a numpy array, need to transform to dataarray
            _csrh = xr.DataArray(_csrh_int, name="csrh", dims=("lev", "lat"), 
                                  coords={"lev": lev, "lat": _csrh.lat})
        return _csrh

    # 5 panel plot for 5 domain averages ordered from South to North
    fig=plt.figure(figsize=(16,4))
    # we also collect the models' domain averages to plot the model median and std dev across models
    shext = list()
    shsub = list()
    trops = list()
    nhsub = list()
    nhext = list()
    for mod in models:
        csrh = hlp.load_csrh_zontimmean(mod, exp)
        csrh = _height2pressurelevels(csrh, mod, exp)
        # 70S - 35S
        ax=plt.subplot(1,5,1)     
        _plot_csrh_singledata(hlp.compute_domainmean(csrh, lats=-70, latn=-35), mod=mod)
        # 35S - 15S
        ax=plt.subplot(1,5,2)     
        _plot_csrh_singledata(hlp.compute_domainmean(csrh, lats=-35, latn=-15), mod=mod)
        # 15S - 15N
        ax=plt.subplot(1,5,3)     
        _plot_csrh_singledata(hlp.compute_domainmean(csrh, lats=-15, latn=+15), mod=mod)
        # 15N - 35N
        ax=plt.subplot(1,5,4)     
        _plot_csrh_singledata(hlp.compute_domainmean(csrh, lats=+15, latn=+35), mod=mod)
        # 35N - 70N
        ax=plt.subplot(1,5,5)     
        _plot_csrh_singledata(hlp.compute_domainmean(csrh, lats=+35, latn=+70), mod=mod)
        # save domain averages
        shext.append(hlp.compute_domainmean(csrh, lats=-70, latn=-35).values)
        shsub.append(hlp.compute_domainmean(csrh, lats=-35, latn=-15).values)
        trops.append(hlp.compute_domainmean(csrh, lats=-15, latn=+15).values)
        nhsub.append(hlp.compute_domainmean(csrh, lats=+15, latn=+35).values)
        nhext.append(hlp.compute_domainmean(csrh, lats=+35, latn=+70).values)

    # include model median and std dev across models for amip simulations
    # make x-axis labels nice by hand for amip (but not the other simulations)
    if exp=="amip":
        ax=plt.subplot(1,5,1); plt.plot(np.nanmedian(np.array(shext), axis=0), csrh.lev/100, color="black", linewidth=2);
        plt.xlabel("model median", fontsize=10, fontweight="bold", color="black", loc="right")
        plt.xticks(ticks=[-1,-0.5,0,0.5], labels=["-1.0","-0.5","0","0.5"])
        ax=plt.subplot(1,5,2); plt.plot(np.nanmedian(np.array(shsub), axis=0), csrh.lev/100, color="black", linewidth=2);
        plt.xticks(ticks=[-1,-0.5,0,0.5], labels=["-1.0","-0.5","0","0.5"])
        ax=plt.subplot(1,5,3); plt.plot(np.nanmedian(np.array(trops), axis=0), csrh.lev/100, color="black", linewidth=2);
        plt.xticks(ticks=[-0.5,0,0.5], labels=["-0.5","0","0.5"])
        ax=plt.subplot(1,5,4); plt.plot(np.nanmedian(np.array(nhsub), axis=0), csrh.lev/100, color="black", linewidth=2) 
        plt.xticks(ticks=[-0.5,0,0.5], labels=["-0.5","0","0.5"])
        ax=plt.subplot(1,5,5); plt.plot(np.nanmedian(np.array(nhext), axis=0), csrh.lev/100, color="black", linewidth=2)   
        plt.xticks(ticks=[-0.5,-0.25,0,0.25,0.5], labels=["-0.5","-0.25","0","0.25","0.5"])

    # make nice axes for subplots
    ax=plt.subplot(1,5,1); _make_niceaxes(ax); plt.title("70S - 35S", fontweight="bold", fontsize=15)
    plt.ylabel(r"pressure / hPa", size=12, loc="top")
    plt.yticks([1000,800,600,400,200,10], ["1000", "800", "600", "400", "200", "10"], fontsize=10) 
    ax=plt.subplot(1,5,2); _make_niceaxes(ax); plt.title("35S - 15S", fontweight="bold", fontsize=15)
    ax=plt.subplot(1,5,3); _make_niceaxes(ax); plt.title("15S - 15N", fontweight="bold", fontsize=15); 
    ax=plt.subplot(1,5,4); _make_niceaxes(ax); plt.title("15N - 35N", fontweight="bold", fontsize=15)
    ax=plt.subplot(1,5,5); _make_niceaxes(ax); plt.title("35N - 70N", fontweight="bold", fontsize=15)
    plt.xlabel(r"clear-sky radiative heating / K$\,$day$^{-1}$", size=12, loc="right")

    plt.subplots_adjust(left=0.1, bottom=0.15, right=0.9, top=0.9,
                        wspace=0.08, hspace=0.1)

#----------------------------------------------------------------------
# Generates plots by calling plotting functions.
#----------------------------------------------------------------------

# xypanel plots of zonal means
if lplot1:
    # amip
    plot_csrh_xypanelplot(exp="amip", models=hlp.models_amip)
    plt.savefig("./figures/csrh_zonaltimemean_xypanelplot_amip.pdf")
    # amip plus 4K SST
    plot_csrh_xypanelplot(exp="amip-p4K", models=hlp.models_amipp4K)
    plt.savefig("./figures/csrh_zonaltimemean_xypanelplot_amipp4K.pdf")
    # amip with 4K-future SST increase
    plot_csrh_xypanelplot(exp="amip-future4K", models=hlp.models_amipfuture4K)
    plt.savefig("./figures/csrh_zonaltimemean_xypanelplot_amipfuture4K.pdf")

# 3 approaches
if lplot2:
    # amip
    plot_csrh_3approaches(exp="amip", models=hlp.models_amip)
    plt.savefig("./figures/csrh_zonaltimemean_3approaches_amip.pdf")
    # amip plus 4K SST
    plot_csrh_3approaches(exp="amip-p4K", models=hlp.models_amipp4K)
    plt.savefig("./figures/csrh_zonaltimemean_3approaches_amip-p4K.pdf")
    # amip with 4K-future SST increase
    plot_csrh_3approaches(exp="amip-future4K", models=hlp.models_amipfuture4K)
    plt.savefig("./figures/csrh_zonaltimemean_3approaches_amip-future4K.pdf")

# domain averages
if lplot3:
    # amip
    plot_csrh_5domains(exp="amip", models=hlp.models_amip)
    plt.savefig("figures/csrh_5xdomaintimemean_amip.pdf")
    # amip-p4K
    plot_csrh_5domains(exp="amip-p4K", models=hlp.models_amipp4K)
    plt.savefig("figures/csrh_5xdomaintimemean_amip-p4K.pdf")
    # amip with 4K-future SST increase
    plot_csrh_5domains(exp="amip-future4K", models=hlp.models_amipfuture4K)
    plt.savefig("figures/csrh_5xdomaintimemean_amip-future4K.pdf")