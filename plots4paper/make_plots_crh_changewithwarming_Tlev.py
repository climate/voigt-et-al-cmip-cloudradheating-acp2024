#!/usr/bin/env python
# coding: utf-8

# Plots of CRH change with surface warming sampled with T as vertical coordinate for all available models. 
# Change is calculated as amip-p4K - amip and amip-future4K - amip.
# Generates the following types of multi-panel plots of CRH:
#   * Plot 1: one panel per model, using only one CRH approach per model

# on srvx1 of img univie, call as follows:
# /home/swd/manual/nwp/2023.1/bin/python3.10 make_plots_crh_changewithwarming_Tlev.py

import os
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt 
import matplotlib as mpl
import helpers as hlp

#----------------------------------------------------------------------
# Defines switches re which plots should be generated
#----------------------------------------------------------------------
lplot1=True

#----------------------------------------------------------------------
# Defines plotting functions
#----------------------------------------------------------------------

# Plot 1:
# zonal-mean time-mean CRH for all available models of a given simulation in a 4-column panel plot
def plot_dcrh_Tlev_xypanelplot(exp, models):

    # temperature levels
    Tint=np.linspace(100,320,221)

    crh_clev=0.5*np.delete(1.0*np.linspace(-1,1,21),10) # contour levels for CRH, remove 0
    nmod = len(models) # number of models

    ncol=4
    # number of panel rows as a function of number of models, we add one extra row to place colorbar there
    nrow=int(np.floor(nmod/ncol)) + 1
    if np.mod(nmod,ncol) > 0: nrow+=1

    def _plot_crh_singledata(crh, ax):
        """ Plots zonal-mean time-mean CRH for T as vertical coordinate."""
        cnt=plt.contourf(crh.lat, crh.Tint, crh, crh_clev, cmap="RdBu_r", extend="both")
        for c in cnt.collections: c.set_edgecolor("face")
        plt.ylim(300,180)
        plt.yticks([300,270,240,210,180],[" ", " ", " ", " ", " "], size=10);
        plt.xlim(-88,88)
        plt.xticks([-60,-30,0,30,60],[" ", " ", " ", " ", " "], size=10, va="top");
        ax.tick_params(direction="in", length=4)
        return

    # we use one extra row to place the colorbar there
    fig=plt.figure(figsize=(4*6,nrow*4))

    # loop over models, use crh in following order: 1) from CFmon flux, 2) from tntr of CFmon, AERmon, Emon, 3) from tntr of EmonZ
    counter=0
    for mod in models:
        counter+=1
        crh_exp = hlp.crh_zonaltimemean_Tlevels(mod, exp=exp, Tint=Tint)
        crh_ami = hlp.crh_zonaltimemean_Tlevels(mod, exp="amip", Tint=Tint)
        crh_ami2= hlp.crh_zonaltimemean_Tlevels(mod, exp="amip", Tint=Tint, remove_pbl=True)
        ax=plt.subplot(nrow, ncol, counter)
        _plot_crh_singledata(crh_exp-crh_ami, ax)
        # overlay with contour lines of crh in amip simulation with pbl removed
        plt.contour(crh_ami2.lat, crh_ami2.Tint, crh_ami2, levels=2*crh_clev, colors="k")
        if np.mod(counter, ncol) == 1:
            plt.ylabel("temperature / K", loc="top", fontsize=15)
            plt.yticks([300,270,240,210,180],["300","270","240","210","180"], size=12);
        if counter>ncol*(nrow-2):
            plt.xlabel("latitude / deg", loc="right", fontsize=15)
            plt.xticks([-60,-30,0,30,60],["60S", "30S","Eq","30N","60N"], size=12, va="top");
        # for rows that are not completely filled, we need to include
        # xlabel in the panels of the row above
        if counter+ncol>nmod: 
            plt.xlabel("latitude / deg", loc="right", fontsize=15)
            plt.xticks([-60,-30,0,30,60],["60S", "30S","Eq","30N","60N"], size=12, va="top");
        ax.text(0.03,0.96, mod, ha="left", va="top", transform=ax.transAxes, backgroundcolor="white", 
                size=15, bbox=dict(facecolor="white", edgecolor="k", boxstyle="round,pad=0.3"))       

    # plot colorbar
    # solution taken and adapted from https://stackoverflow.com/a/62436015
    # axes position found manually, to do so use "print(ax)" for above subplots
    ax = fig.add_axes([0.714,0.381765,0.185, 0.04/nrow])
    cb = mpl.colorbar.ColorbarBase(ax, orientation="horizontal", 
                                   cmap="RdBu_r", extend="both", ticks=[-1, -0.7, -0.4, 0.0, 0.4, 0.7, 1.0],
                                   label="K/day",
                                   boundaries=crh_clev, norm=mpl.colors.Normalize(crh_clev[0], crh_clev[-1]))
    cb.ax.set_xticklabels(["-1.0", "-0.7", "-0.4", "0", "0.4", "0.7", "1.0"])
    cb.ax.tick_params(labelsize=15)
    cb.set_label(label="K/day", size=15)

    hlp.spacing_subplots()

#----------------------------------------------------------------------
# Generates plots by calling plotting functions.
#----------------------------------------------------------------------

# xypanel plots of zonal means
if lplot1:
    # amip plus 4K SST
    plot_dcrh_Tlev_xypanelplot(exp="amip-p4K", models=hlp.models_amipp4K)
    plt.savefig("./figures/crh_changewithwarming_Tlev_zonaltimemean_xypanelplot_amipp4K-amip.pdf")
    # amip with 4K-future SST increase
    plot_dcrh_Tlev_xypanelplot(exp="amip-future4K", models=hlp.models_amipfuture4K)
    plt.savefig("./figures/crh_changewithwarming_Tlev_zonaltimemean_xypanelplot_amipfuture4K-amip.pdf")