#!/usr/bin/env python
# coding: utf-8

# Plots of CRH change with surface warming sampled with pressure as vertical coordinate for all available models. 
# Change is calculated as amip-p4K - amip and amip-future4K - amip.
# Generates the following types of multi-panel plots of CRH:
#   * Plot 1: one panel per model, using only one CRH approach per model;
#   * Plot 2: vertical profiles of CRH averaged over 5 domains

# on srvx1 of img univie, call as follows:
# /home/swd/manual/nwp/2023.1/bin/python3.10 make_plots_crh_changewithwarming.py

import os
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt 
import matplotlib as mpl
import helpers as hlp

#----------------------------------------------------------------------
# Defines switches re which plots should be generated
#----------------------------------------------------------------------
lplot1=True
lplot2=False

#----------------------------------------------------------------------
# Defines plotting functions
#----------------------------------------------------------------------

# Plot 1:
# zonal-mean time-mean CRH change for all available models of a given simulation in a 4-column panel plot
# and with tropopause and T=0 and T=-38deg C as lines
def plot_dcrh_xypanelplot(exp, models):

    crh_clev=np.delete(1*np.linspace(-1,1,21),10) #np.array([-2.0,-1.5,-1.0,-0.8,-0.6,-0.4,-0.2,0.2,0.4,0.6,0.8,1.0,1.5,2.0])
    #crh_clev=np.array([-4.0,-3.5,-3.0,-2.5,-2.0,-1.5,-1.0,-0.5,-0.4,-0.3,-0.2,-0.1,0.1,0.2,0.3,0.4,0.5,1.0,1.5,2.0,2.5,3.0,3.5,4.0])
    dcrh_clev=np.delete(0.5*np.linspace(-1,1,21),10) # contour levels for dCRH, remove 0
    nmod = len(models) # number of models

    ncol=4
    # number of panel rows as a function of number of models, we add one extra row to place colorbar there
    nrow=int(np.floor(nmod/ncol)) + 1
    if np.mod(nmod,ncol) > 0: nrow+=1

    def _plot_crh_singledata(crh, ax, clev, plottype="contourf", labove700hPa=False):
        """ Plots zonal-mean time-mean CRH. Works for both pressure-level and height-level data."""
        lat=crh.lat
        if "lev" in crh.coords: # pressure levels (almost all models)
            lev = crh.lev.values
        elif "hgt" in crh.coords: # height levels (HadGEM and UKESM models)
            aux, lev = hlp.height2pressure_zg(crh,mod,exp)
            crh = xr.DataArray(aux, name="crh", dims=("lev", "lat"), 
                           coords={"lev": lev, "lat": lat})
        if labove700hPa:
            # only plot crh from amip above 600 hPa
            crh = crh.where(crh.lev<700e2)
        if plottype=="contourf":
            cnt=plt.contourf(lat, lev/100, crh, clev, cmap="RdBu_r", extend="both")
            for c in cnt.collections: c.set_edgecolor("face")
        if plottype=="contour":
            plt.contour(lat, lev/100, crh, clev, colors="dimgray", linewidths=0.5)
        plt.ylim(1000,10)
        plt.yticks([800,600,400,200],[" ", " ", " ", " "], size=12);
        plt.xlim(-88,88)
        plt.xticks([-60,-30,0,30,60],[" ", " ", " ", " ", " "], size=12, va="top");
        ax.tick_params(direction="in", length=6)
        return 

    def _plot_ta_tp(ta, tp, ax, linestyle="-"):
        """ Plots temperature contours and tropopause, which are both on pressure levels."""
        plt.contour(ta.lat, ta.lev/100, ta, levels=[273.15-38, 273.15], colors="darkgray", linestyles=linestyle, linewidths=0.5)
        plt.plot(tp.lat, tp, color="seagreen", linestyle=linestyle, linewidth=0.5)
        return

    # we use one extra row to place the colorbar there
    fig=plt.figure(figsize=(6*4,nrow*4))
    
    # loop over models, use crh in following order: 1) from CFmon flux, 2) from tntr of CFmon, AERmon, Emon, 3) from tntr of EmonZ
    counter=0
    for mod in models:
        counter+=1
        lplot=False # plot already done for the model?
        # air temperature and tropopause
        ta_ami = hlp.ta_Amon_ymonmean(mod, exp="amip")["ta"].mean(["month", "lon"]).squeeze()
        tp_ami = hlp.tropo_Amon_zontimmean(mod, exp="amip")["trop"].squeeze()
        # crh
        crh_exp = hlp.load_crh_zontimmean(mod, exp)
        crh_ami = hlp.load_crh_zontimmean(mod, exp="amip")
        dcrh = crh_exp - crh_ami
        # plot
        ax=plt.subplot(nrow, ncol, counter)
        _plot_crh_singledata(dcrh, ax, clev=dcrh_clev, plottype="contourf")
        #_plot_crh_singledata(crh_ami, ax, clev=crh_clev, plottype="contour",labove700hPa=True)
        _plot_ta_tp(ta_ami, tp_ami, ax, linestyle="-")
        if np.mod(counter, ncol) == 1:
            plt.ylabel("pressure / hPa", loc="top", fontsize=15)
            plt.yticks([800,600,400,200],["800", "600","400","200"], size=12);
        if counter>ncol*(nrow-2):
            plt.xlabel("latitude / deg", loc="right", fontsize=15)
            plt.xticks([-60,-30,0,30,60],["60S", "30S","Eq","30N","60N"], size=12, va="top");
        # for rows that are not completely filled, we need to include
        # xlabel in the panels of the row above
        if counter+ncol>nmod: 
            plt.xlabel("latitude / deg", loc="right", fontsize=15)
            plt.xticks([-60,-30,0,30,60],["60S", "30S","Eq","30N","60N"], size=12, va="top");
        ax.text(0.03,0.96, mod, ha="left", va="top", transform=ax.transAxes, backgroundcolor="white", 
                size=15, bbox=dict(facecolor="white", edgecolor="k", boxstyle="round,pad=0.3"))       
  
    # plot colorbar
    # solution taken and adapted from https://stackoverflow.com/a/62436015
    # axes position found manually, to do so use "print(ax)" for above subplots
    if exp=="amip-p4K" or exp=="amip-future4K":
        ax = fig.add_axes([0.714,0.381765,0.185, 0.04/nrow])
    cb = mpl.colorbar.ColorbarBase(ax, orientation="horizontal", 
                                   cmap="RdBu_r", extend="both", ticks=[-0.5, -0.25, 0, 0.25, 0.5],
                                   label="K/day",
                                   boundaries=dcrh_clev, norm=mpl.colors.Normalize(dcrh_clev[0], dcrh_clev[-1]))     
    cb.ax.set_xticklabels(["-0.5", "-0.25", "0", "0.25", "0.5"])
    cb.ax.tick_params(labelsize=15)
    cb.set_label(label="K/day", size=15)
    
    hlp.spacing_subplots()

    
# Plot 2:
# 5 panel plot for 5 domain averages ordered from South to North
def plot_dcrh_5domains(exp, models, beta=1.2):
    """ Makes multipanel plots of domain averaged CRH change in exp minus amip. Includes
        prediction by upward shifted amip CRH, where beta defines the magnitude of the
        upward shift. 
        
        First row is for all models. Rows below for individual models. """
    
    nmod = len(models) # number of models
    
    def _make_niceaxes(ax, xlim=[-1,1]):
        # adjust spines
        ax.spines["top"].set_color("none")
        ax.spines["right"].set_color("none")
        ax.spines["left"].set_color("none")
        ax.xaxis.set_ticks_position("bottom")
        ax.spines["bottom"].set_position(("data",1000))
        ax.spines["right"].set_position(("data",xlim[1]+0.03))
        ax.yaxis.tick_right()
        ax.yaxis.set_label_position("right")
        plt.xticks([-0.5,-0.4,-0.3,-0.2,-0.1,0,0.1,0.2,0.3,0.4,0.5], ["","","","","","","","","","",""])
        plt.xlim(xlim[0], xlim[1])
        plt.ylim(1000,10)
        plt.yticks([ ], fontsize=10) 
        plt.plot([0,0], [1000,10], linewidth=0.7, color="k", zorder=-10)
        ticklength=0.04*np.abs(xlim[1]-xlim[0])
        for ypos in [1000,800,600,400,200,10]:
            plt.plot([-0.5*ticklength,0.5*ticklength], [ypos,ypos], linewidth=0.7, color="k", zorder=-10)

    def _plot_crh_singledata(_crh):
        """ Plots domain-mean time-mean CRH. Works for both pressure-level and height-level data."""
        plt.plot(_crh, _crh.lev/100, color="gray", label=mod)
        plt.yticks([800,600,400,200],["800", "600","400","200"], size=6);
        return
        
    def _dcrh_upwardshift(_crh, _beta):
        """ Predicts CRH change based on upward shift of reference crh.
            Returns xarray datarray.
        """
        _lev = _crh.lev
        # do we need to reverse vertical axis so that pressure is increasing?
        lreverse=False
        if _lev[0]>_lev[1]:
            lreverse=True
            _lev = _lev[::-1]
            _crh = _crh[::-1]
        # interpolate shifted crh from levels lev/beta to input levels lev
        _crh_shift = np.interp(_lev, _lev/_beta, _crh) 
        # reverse pressure back if reversed previously
        if lreverse: 
            _crh = _crh[::-1]
            _crh_shift = _crh_shift[::-1]
        return _crh_shift - _crh
    
    def _plot_crh_upwardshift(_crh, beta):
        """ Shift reference CRH upward from pressue lev to pressure lev/beta and plot."""
        _dcrh = _dcrh_upwardshift(_crh, beta)
        # restrict plot to levels above 600 hPa
        _dcrh = _dcrh.sel(lev=slice(600e2, 0))
        plt.plot(_dcrh, _dcrh.lev/100, color="royalblue")
        return 
    
    def _compute_correlation_upwardshift(_dcrh, _crh, beta):
        """ Computes Pearson correlation between actual CRH change and prediction by
            upward shift of reference crh according to beta."""
        # prediction by upward shift
        _dcrh_shift = _dcrh_upwardshift(_crh, beta)
        # restrict to levels between 500 and 100 600 hPa
        _dcrh = _dcrh.sel(lev=slice(500e2, 100e2))
        _dcrh_shift = _dcrh_shift.sel(lev=slice(500e2, 100e2))
        # return correlation coefficient
        return np.around(np.corrcoef(_dcrh, _dcrh_shift)[0,1], decimals=2)

    def _convert_crh_hgt2lev(_crh, mod, exp):
        """ Converts crh from common height levels to common pressure levels.
            Needed for HadGEM3-GC31-LL."""
        _crh_int, lev = hlp.height2pressure_zg(_crh, mod=mod, exp="amip")
        # _data_int is a numpy array, need to transform to dataarray
        _crh_int = xr.DataArray(_crh_int, name="crh", dims=("lev", "lat"), 
                            coords={"lev": lev, "lat": _crh.lat})
        return _crh_int    
    
    fig=plt.figure(figsize=(16,3+3*nmod))
    
    # first row: 5 panel plot for 5 domain averages ordered from South to North
    for mod in models:
        crh_exp = hlp.load_crh_zontimmean(mod, exp)
        crh_ami = hlp.load_crh_zontimmean(mod, exp="amip")
        if mod=="HadGEM3-GC31-LL":
            crh_exp = _convert_crh_hgt2lev(crh_exp, mod, exp)
            crh_ami = _convert_crh_hgt2lev(crh_ami, mod, exp="amip")
        dcrh = crh_exp - crh_ami
        # 70S - 35S
        ax=plt.subplot(nmod+1,5,1)     
        _plot_crh_singledata(hlp.compute_domainmean(dcrh, lats=-70, latn=-35))
        # 35S - 15S
        ax=plt.subplot(nmod+1,5,2)  
        if mod=="MRI-ESM2-0" and exp=="amip-p4K":
            # for nicer plot 
            aux = hlp.compute_domainmean(dcrh, lats=-35, latn=-15); aux = xr.where(aux < 0.245, aux, 0.245)
            _plot_crh_singledata(aux)
        else:
            _plot_crh_singledata(hlp.compute_domainmean(dcrh, lats=-35, latn=-15))
        # 15S - 15N
        ax=plt.subplot(nmod+1,5,3)     
        _plot_crh_singledata(hlp.compute_domainmean(dcrh, lats=-15, latn=+15))
        # 15N - 35N
        ax=plt.subplot(nmod+1,5,4)     
        _plot_crh_singledata(hlp.compute_domainmean(dcrh, lats=+15, latn=+35))
        # 35N - 70N
        ax=plt.subplot(nmod+1,5,5)     
        _plot_crh_singledata(hlp.compute_domainmean(dcrh, lats=+35, latn=+70))
    
    # now 1 row for each model for the 5 domain averages and with the prediction by upward CRH shift
    for imod in range(nmod):
        crh_exp = hlp.load_crh_zontimmean(models[imod], exp)
        crh_ami = hlp.load_crh_zontimmean(models[imod], exp="amip")
        if models[imod]=="HadGEM3-GC31-LL":
            crh_exp = _convert_crh_hgt2lev(crh_exp, models[imod], exp)
            crh_ami = _convert_crh_hgt2lev(crh_ami, models[imod], exp="amip")
        dcrh = crh_exp - crh_ami
        # 70S - 35S
        ax=plt.subplot(nmod+1,5,(imod+1)*5+1) 
        _plot_crh_singledata(hlp.compute_domainmean(dcrh, lats=-70, latn=-35))
        _plot_crh_upwardshift(hlp.compute_domainmean(crh_ami, lats=-70, latn=-35), beta=beta)
        corrcoeff=_compute_correlation_upwardshift(hlp.compute_domainmean(dcrh, lats=-70, latn=-35), 
                                                   hlp.compute_domainmean(crh_ami, lats=-70, latn=-35), beta=beta)
        plt.text(1, 0.03, "{0:.2f}".format(corrcoeff),
                 fontsize=12, fontweight="bold", ha="right", transform=ax.transAxes)
        # 35S - 15S
        ax=plt.subplot(nmod+1,5,(imod+1)*5+2)  
        if models[imod]=="MRI-ESM2-0" and exp=="amip-p4K":
            # for nicer plot 
            aux = hlp.compute_domainmean(dcrh, lats=-35, latn=-15); aux = xr.where(aux < 0.245, aux, 0.245)
            _plot_crh_singledata(aux)
        else:
            _plot_crh_singledata(hlp.compute_domainmean(dcrh, lats=-35, latn=-15))
        _plot_crh_upwardshift(hlp.compute_domainmean(crh_ami, lats=-35, latn=-15), beta=beta)
        corrcoeff=_compute_correlation_upwardshift(hlp.compute_domainmean(dcrh, lats=-35, latn=-15), 
                                                   hlp.compute_domainmean(crh_ami, lats=-35, latn=-15), beta=beta)
        plt.text(1, 0.03, "{0:.2f}".format(corrcoeff),
                 fontsize=12, fontweight="bold", ha="right", transform=ax.transAxes)
        # 15S - 15N
        ax=plt.subplot(nmod+1,5,(imod+1)*5+3)     
        _plot_crh_singledata(hlp.compute_domainmean(dcrh, lats=-15, latn=+15))
        _plot_crh_upwardshift(hlp.compute_domainmean(crh_ami, lats=-15, latn=+15), beta=beta)
        corrcoeff=_compute_correlation_upwardshift(hlp.compute_domainmean(dcrh, lats=-15, latn=+15), 
                                                   hlp.compute_domainmean(crh_ami, lats=-15, latn=+15), beta=beta)
        plt.text(1, 0.03, "{0:.2f}".format(corrcoeff),
                 fontsize=12, fontweight="bold", ha="right", transform=ax.transAxes)
        # 15N - 35N
        ax=plt.subplot(nmod+1,5,(imod+1)*5+4)     
        _plot_crh_singledata(hlp.compute_domainmean(dcrh, lats=+15, latn=+35))
        _plot_crh_upwardshift(hlp.compute_domainmean(crh_ami, lats=+15, latn=+35), beta=beta)
        corrcoeff=_compute_correlation_upwardshift(hlp.compute_domainmean(dcrh, lats=+15, latn=+35),   
                                                   hlp.compute_domainmean(crh_ami, lats=+15, latn=+35), beta=beta)
        plt.text(1, 0.03, "{0:.2f}".format(corrcoeff),
                 fontsize=12, fontweight="bold", ha="right", transform=ax.transAxes)                 
        # 35N - 70N
        ax=plt.subplot(nmod+1,5,(imod+1)*5+5)     
        _plot_crh_singledata(hlp.compute_domainmean(dcrh, lats=+35, latn=+70))
        _plot_crh_upwardshift(hlp.compute_domainmean(crh_ami, lats=+35, latn=+70), beta=beta)
        corrcoeff=_compute_correlation_upwardshift(hlp.compute_domainmean(dcrh, lats=+35, latn=+70),   
                                                   hlp.compute_domainmean(crh_ami, lats=+35, latn=+70), beta=beta)
        plt.text(1, 0.03, "{0:.2f}".format(corrcoeff),
                 fontsize=12, fontweight="bold", ha="right", transform=ax.transAxes)
           
    # make nice axes for subplots
    # 75S-35S
    for ipanel in range(nmod+1): ax=plt.subplot(nmod+1,5,5*ipanel+1); _make_niceaxes(ax, xlim=[-0.25,0.25])
    # 35S-15S
    for ipanel in range(nmod+1): ax=plt.subplot(nmod+1,5,5*ipanel+2); _make_niceaxes(ax, xlim=[-0.25,0.25])
    # 15S-15N
    for ipanel in range(nmod+1): ax=plt.subplot(nmod+1,5,5*ipanel+3); _make_niceaxes(ax, xlim=[-0.5,0.5]) 
    # 15NS-35N
    for ipanel in range(nmod+1): ax=plt.subplot(nmod+1,5,5*ipanel+4); _make_niceaxes(ax, xlim=[-0.25,0.25])
    # 35N-75N
    for ipanel in range(nmod+1): ax=plt.subplot(nmod+1,5,5*ipanel+5); _make_niceaxes(ax, xlim=[-0.25,0.25])
    
    # add model names and y-axis labels and ticks
    ax=plt.subplot(nmod+1,5,1)
    plt.text(-0.1, 0.5, "All models", fontsize=15, fontweight="bold", ha="center" , va="center", rotation=90, transform=ax.transAxes)
    for imod in range(nmod):
        ax=plt.subplot(nmod+1,5,5*imod+6)
        plt.text(-0.1, 0.5, models[imod], fontsize=15, fontweight="bold", ha="center" , va="center", rotation=90, transform=ax.transAxes)
    
    # add y-axis labels and ticks on right hand side of plot
    ax=plt.subplot(nmod+1,5,5)
    plt.ylabel(r"pressure / hPa", size=15, loc="top")
    plt.yticks([1000,800,600,400,200,10], ["1000", "800", "600", "400", "200", "10"], fontsize=10)
    for imod in range(nmod):
        ax=plt.subplot(nmod+1,5,5*imod+10)
        plt.yticks([1000,800,600,400,200,10], ["1000", "800", "600", "400", "200", "10"], fontsize=10)
        
    # add domains as titles
    ax=plt.subplot(nmod+1,5,1); plt.text(0.5, 1.1, "70S - 35S", fontsize=15, fontweight="bold", 
                                         ha="center" , va="center", transform=ax.transAxes)
    ax=plt.subplot(nmod+1,5,2); plt.text(0.5, 1.1, "35S - 15S", fontsize=15, fontweight="bold", 
                                         ha="center" , va="center", transform=ax.transAxes)
    ax=plt.subplot(nmod+1,5,3); plt.text(0.5, 1.1, "15S - 15N", fontsize=15, fontweight="bold", 
                                         ha="center" , va="center", transform=ax.transAxes)
    ax=plt.subplot(nmod+1,5,4); plt.text(0.5, 1.1, "15N - 35N", fontsize=15, fontweight="bold", 
                                         ha="center" , va="center", transform=ax.transAxes)
    ax=plt.subplot(nmod+1,5,5); plt.text(0.5, 1.1, "35N - 70N", fontsize=15, fontweight="bold", 
                                         ha="center" , va="center", transform=ax.transAxes)
    
    # add x-labels in last row
    ax=plt.subplot(nmod+1,5,5*nmod+1)
    plt.xticks([-0.2,-0.1,0,0.1,0.2], ["-0.2","","0","","0.2"], fontsize=10)
    ax=plt.subplot(nmod+1,5,5*nmod+2)
    plt.xticks([-0.2,-0.1,0,0.1,0.2], ["-0.2","","0","","0.2"], fontsize=10)
    ax=plt.subplot(nmod+1,5,5*nmod+3)
    plt.xticks([-0.5,-0.4,-0.3,-0.2,-0.1,0,0.1,0.2,0.3,0.4,0.5], ["","-0.4","","-0.2","","0","","0.2","","0.4",""], fontsize=10)
    ax=plt.subplot(nmod+1,5,5*nmod+4)
    plt.xticks([-0.2,-0.1,0,0.1,0.2], ["-0.2","","0","","0.2"], fontsize=10)
    ax=plt.subplot(nmod+1,5,5*nmod+5)
    plt.xticks([-0.2,-0.1,0,0.1,0.2], ["-0.2","","0","","0.2"], fontsize=10)
    plt.xlabel(r"cloud-radiative heating / K$\,$day$^{-1}$", size=15, loc="right")
    
    hlp.spacing_subplots()
 
    
#----------------------------------------------------------------------
# Generates plots by calling plotting functions.
#----------------------------------------------------------------------

# xypanel plots of zonal means
if lplot1:
    # amip plus 4K SST
    plot_dcrh_xypanelplot(exp="amip-p4K", models=hlp.models_amipp4K)
    plt.savefig("./figures/crh_changewithwarming_zonaltimemean_xypanelplot_amipp4K-amip.pdf")
    # amip with 4K-future SST increase
    plot_dcrh_xypanelplot(exp="amip-future4K", models=hlp.models_amipfuture4K)
    plt.savefig("./figures/crh_changewithwarming_zonaltimemean_xypanelplot_amipfuture4K-amip.pdf")

# domain averages
if lplot2:
    # amip-p4K
    plot_dcrh_5domains(exp="amip-p4K", models=hlp.models_amipp4K)
    plt.savefig("figures/crh_changewithwarming_5xdomaintimemean_amipp4K-amip.pdf")
    # amip with 4K-future SST increase
    plot_dcrh_5domains(exp="amip-future4K", models=hlp.models_amipfuture4K)
    plt.savefig("figures/crh_changewithwarming_5xdomaintimemean_amipfuture4K-amip.pdf")