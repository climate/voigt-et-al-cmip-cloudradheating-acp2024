#!/usr/bin/env python
# coding: utf-8

# Plots of cloud cover cl for all available models and the amip, amip-p4K and amip-future4K simulations
# Generates the following types of multi-panel plots of cl:
#   * Plot 1: one panel per model
#   * Plot 2: vertical profiles of cl averaged over 5 domains

# on masterhub of img univie, call as follows:
# /home/swd/manual/nwp/2023.1/bin/python3.10 make_plots_cl.py

import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib as mpl
import helpers as hlp

from cmcrameri import cm as cmc    # nice colorbars for cloud fraction; adapted from Bertrand et al. 2024 colorbar for cloud fraction
                                   # see https://github.com/bertrandclim/essd2023/blob/v1.0/9-9_ESSD-figs-export_R2R_oct26.ipynb

import warnings
warnings.simplefilter("ignore") 

#----------------------------------------------------------------------
# Defines switches re which plots should be generated
#----------------------------------------------------------------------
lplot1=True
lplot2=False

#----------------------------------------------------------------------
# Defines plotting functions
#----------------------------------------------------------------------

# Plot 1:
# zonal-mean time-mean CRH for all available models of a given simulation in a 4-column panel plot
# and with tropopause and T=0 and T=-38deg C as lines
def plot_cl_xypanelplot(exp, models):

    # contour levels and colorbar
    cl_clev=np.arange(0.,0.7,0.05)
    cmap=cmc.batlow

    nmod = len(models) # number of models

    ncol=4
    # number of panel rows as a function of number of models, we add one extra row to place colorbar there
    nrow=int(np.floor(nmod/ncol)) + 1
    if np.mod(nmod,ncol) > 0: nrow+=1

    def _plot_ta_tp(ta, tp, ax):
        """ Plots temperature contours and tropopause, which are both on pressure levels."""
        plt.contour(ta.lat, ta.lev/100, ta, levels=[273.15-38, 273.15], colors="darkgray", linestyles="-", linewidths=1)
        plt.plot(tp.lat, tp, color="white", linewidth=1)
        return

    def _plot_cl_singledata(cl, ax):
        """ Plots zonal-mean time-mean cloud cover. Works for both pressure-level and height-level data."""
        lat=cl.lat
        if "lev" in cl.coords: # pressure levels (almost all models)
            lev = cl.lev.values
        elif "hgt" in cl.coords: # height levels (HadGEM and UKESM models)
            cl, lev = hlp.height2pressure_zg(cl,mod,exp)
        cnt=plt.contourf(lat, lev/100, 0.01*cl, cl_clev, cmap=cmap)
        for c in cnt.collections: c.set_edgecolor("face")
        plt.ylim(1000,10)
        plt.yticks([800,600,400,200],[" ", " ", " ", " "], size=12);
        plt.xlim(-88,88)
        plt.xticks([-60,-30,0,30,60],[" ", " ", " ", " ", " "], size=12, va="top");
        ax.tick_params(direction="in", length=6)
        return

    # we use one extra row to place the colorbar there
    fig=plt.figure(figsize=(6*4,nrow*4))

    # loop over models
    counter=0
    for mod in models:
        counter+=1
        # air temperature and tropopause
        ta = hlp.ta_Amon_ymonmean(mod, exp)["ta"].mean(["month", "lon"]).squeeze()
        tp = hlp.tropo_Amon_zontimmean(mod, exp)["trop"].squeeze()
        # cl
        cl = hlp.load_cl_zontimmean(mod, exp)
        if mod in ["CESM2", "CESM2-FV2", "CESM2-WACCM", "CESM2-WACCM-FV2"]:
            if cl is not None: cl = cl.where(cl.lev<500e2)
            if ta is not None: ta = ta.where(ta.lev<500e2) 
        # leave this iteration and go to next model if there is no cloud cover data
        if cl is None: continue
        # also for CESM2, CESM2-WACCM
        if mod in ["CESM2", "CESM2-WACCM"]: continue
        ax=plt.subplot(nrow, ncol, counter)
        _plot_cl_singledata(cl, ax)
        _plot_ta_tp(ta, tp, ax)
        if np.mod(counter, ncol) == 1:
            plt.ylabel("pressure / hPa", loc="top", fontsize=15)
            plt.yticks([800,600,400,200],["800", "600","400","200"], size=12);
        if counter>ncol*(nrow-2):
            plt.xlabel("latitude / deg", loc="right", fontsize=15)
            plt.xticks([-60,-30,0,30,60],["60S", "30S","Eq","30N","60N"], size=12, va="top");
        # for amip, need to add y-xis label for CNRM-CM6-1 by hand
        if exp=="amip" and mod=="CNRM-CM6-1":
            plt.ylabel("pressure / hPa", loc="top", fontsize=15)
            plt.yticks([800,600,400,200],["800", "600","400","200"], size=12);            
        # for rows that are not completely filled, we need to include
        # xlabel in the panels of the row above
        if counter+ncol>nmod: 
            plt.xlabel("latitude / deg", loc="right", fontsize=15)
            plt.xticks([-60,-30,0,30,60],["60S", "30S","Eq","30N","60N"], size=12, va="top");
        ax.text(0.03,0.96, mod, ha="left", va="top", transform=ax.transAxes, backgroundcolor="white", 
                size=15, bbox=dict(facecolor="white", edgecolor="k", boxstyle="round,pad=0.3"))       

    # plot colorbar
    # solution taken and adapted from https://stackoverflow.com/a/62436015
    # axes position found manually, to do so use "print(ax)" for above subplots
    if exp=="amip-p4K" or exp=="amip-future4K":
        ax = fig.add_axes([0.714,0.381765,0.185, 0.04/nrow])
    if exp=="amip":
        ax = fig.add_axes([0.714,0.20,0.185, 0.04/nrow])
    cb = mpl.colorbar.ColorbarBase(ax, orientation="horizontal", 
                                   cmap=cmap, extend="both", ticks=[0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6],
                                   label="",
                                   boundaries=cl_clev, norm=mpl.colors.Normalize(cl_clev[0], cl_clev[-1]))
    cb.ax.set_xticklabels(["0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6"])
    cb.ax.tick_params(labelsize=15)
    cb.set_label(label="", size=15)

    hlp.spacing_subplots()

# Plot 2:
# 5 panel plot for 5 domain averages ordered from South to North
def plot_cl_5domains(exp, models):

    nmod = len(models) # number of models

    def _make_niceaxes(ax, xlim, ticklength=0.08):
        # adjust spines
        ax.spines["top"].set_color("none")
        ax.spines["right"].set_color("none")
        ax.xaxis.set_ticks_position("bottom")
        ax.spines["bottom"].set_position(("data",1000))
        ax.spines["left"].set_position(("data",-2))
        ax.spines["left"].set_color("none")
        plt.ylim(1000,10)
        plt.xlim(xlim[0], xlim[1])
        plt.yticks([ ], fontsize=10) 
        #for ypos in [1000,800,600,400,200,10]:
        #    plt.plot([-0.5*ticklength,0.5*ticklength], [ypos,ypos], linewidth=0.7, color="k", zorder=-10)

    def _plot_cl_singledata(_cl, mod):
        """ Plots domain-mean time-mean CRH. Assumes pressure levels."""
        plt.plot(_cl, _cl.lev/100, color="gray", label=mod)
        plt.yticks([800,600,400,200],["800", "600","400","200"], size=6);
        return

    def _height2pressurelevels(_cl, mod, exp):
        """ Interpolates from height to common pressure levels for HadGEM and UKESM models."""
        if mod in ["HadGEM3-GC31-LL", "HadGEM3-GC31-MM", "UKESM1-0-LL"]:
            _cl_int, lev = hlp.height2pressure_zg(_cl, mod, exp)
            # _cl_int is a numpy array, need to transform to dataarray
            _cl = xr.DataArray(_cl, name="cl", dims=("lev", "lat"), 
                                  coords={"lev": lev, "lat": _cl.lat})
        return _cl

    # 5 panel plot for 5 domain averages ordered from South to North
    fig=plt.figure(figsize=(16,4))
    # we also collect the models' domain averages to plot the model median and std dev across models
    shext = list()
    shsub = list()
    trops = list()
    nhsub = list()
    nhext = list()
    for mod in models:
        cl = hlp.load_cl_zontimmean(mod, exp)
        if mod in ["CESM2", "CESM2-FV2", "CESM2-WACCM", "CESM2-WACCM-FV2"]:
            if cl is not None: cl = cl.where(cl.lev<500e2)
        # leave this iteration and go to next model if there is no cloud cover data
        if cl is None: continue
        # also for CESM2, CESM2-WACCM
        if mod in ["CESM2", "CESM2-WACCM"]: continue
        cl = _height2pressurelevels(cl, mod, exp)
        # 70S - 35S
        ax=plt.subplot(1,5,1)     
        _plot_cl_singledata(hlp.compute_domainmean(cl, lats=-70, latn=-35), mod=mod)
        # 35S - 15S
        ax=plt.subplot(1,5,2)     
        _plot_cl_singledata(hlp.compute_domainmean(cl, lats=-35, latn=-15), mod=mod)
        # 15S - 15N
        ax=plt.subplot(1,5,3)     
        _plot_cl_singledata(hlp.compute_domainmean(cl, lats=-15, latn=+15), mod=mod)
        # 15N - 35N
        ax=plt.subplot(1,5,4)     
        _plot_cl_singledata(hlp.compute_domainmean(cl, lats=+15, latn=+35), mod=mod)
        # 35N - 70N
        ax=plt.subplot(1,5,5)     
        _plot_cl_singledata(hlp.compute_domainmean(cl, lats=+35, latn=+70), mod=mod)
        # save domain averages
        shext.append(hlp.compute_domainmean(cl, lats=-70, latn=-35).values)
        shsub.append(hlp.compute_domainmean(cl, lats=-35, latn=-15).values)
        trops.append(hlp.compute_domainmean(cl, lats=-15, latn=+15).values)
        nhsub.append(hlp.compute_domainmean(cl, lats=+15, latn=+35).values)
        nhext.append(hlp.compute_domainmean(cl, lats=+35, latn=+70).values)

    # include model median and std dev across models, make x-axis labels nice by hand
    ax=plt.subplot(1,5,1); plt.plot(np.nanmedian(np.array(shext), axis=0), cl.lev/100, color="black", linewidth=2);
    #plt.xlabel("model median", fontsize=10, fontweight="bold", color="black", loc="right")
    plt.xticks(ticks=[0,10,20,30,40], labels=[0,10,20,30,40])
    ax=plt.subplot(1,5,2); plt.plot(np.nanmedian(np.array(shsub), axis=0), cl.lev/100, color="black", linewidth=2);
    plt.xticks(ticks=[0,10,20], labels=[0,10,20])
    ax=plt.subplot(1,5,3); plt.plot(np.nanmedian(np.array(trops), axis=0), cl.lev/100, color="black", linewidth=2);
    plt.xticks(ticks=[0,10,20,30], labels=[0,10,20,30])
    ax=plt.subplot(1,5,4); plt.plot(np.nanmedian(np.array(nhsub), axis=0), cl.lev/100, color="black", linewidth=2) 
    plt.xticks(ticks=[0,10,20], labels=[0,10,20])
    ax=plt.subplot(1,5,5); plt.plot(np.nanmedian(np.array(nhext), axis=0), cl.lev/100, color="black", linewidth=2)   
    plt.xticks(ticks=[0,10,20,30,40], labels=[0,10,20,30,40])

    # make nice axes for subplots
    ax=plt.subplot(1,5,1); _make_niceaxes(ax, xlim=[0,46]); plt.title("70S - 35S", fontweight="bold", fontsize=15)
    plt.ylabel(r"pressure / hPa", size=12, loc="top")
    plt.yticks([1000,800,600,400,200,10], ["1000", "800", "600", "400", "200", "10"], fontsize=10) 
    ax=plt.subplot(1,5,2); _make_niceaxes(ax, xlim=[0,20]); plt.title("35S - 15S", fontweight="bold", fontsize=15)
    ax=plt.subplot(1,5,3); _make_niceaxes(ax, xlim=[0,30]); plt.title("15S - 15N", fontweight="bold", fontsize=15); 
    ax=plt.subplot(1,5,4); _make_niceaxes(ax, xlim=[0,20]); plt.title("15N - 35N", fontweight="bold", fontsize=15)
    ax=plt.subplot(1,5,5); _make_niceaxes(ax, xlim=[0,40]); plt.title("35N - 70N", fontweight="bold", fontsize=15)
    plt.xlabel("cloud cover / %", size=12, loc="right")

    plt.subplots_adjust(left=0.1, bottom=0.15, right=0.9, top=0.9,
                        wspace=0.08, hspace=0.1)

#----------------------------------------------------------------------
# Generates plots by calling plotting functions.
#----------------------------------------------------------------------

# xypanel plots of zonal means
if lplot1:
    # amip
    plot_cl_xypanelplot(exp="amip", models=hlp.models_amip)
    plt.savefig("./figures/cl_zonaltimemean_xypanelplot_amip.pdf")
    # amip plus 4K SST
    plot_cl_xypanelplot(exp="amip-p4K", models=hlp.models_amipp4K)
    plt.savefig("./figures/cl_zonaltimemean_xypanelplot_amipp4K.pdf")
    # amip with 4K-future SST increase
    plot_cl_xypanelplot(exp="amip-future4K", models=hlp.models_amipfuture4K)
    plt.savefig("./figures/cl_zonaltimemean_xypanelplot_amipfuture4K.pdf")

# domain averages
if lplot2:
    # amip
    plot_cl_5domains(exp="amip", models=hlp.models_amip)
    plt.savefig("figures/cl_5xdomaintimemean_amip.pdf")
    # amip-p4K
    plot_cl_5domains(exp="amip-p4K", models=hlp.models_amipp4K)
    plt.savefig("figures/cl_5xdomaintimemean_amip-p4K.pdf")
    # amip with 4K-future SST increase
    plot_cl_5domains(exp="amip-future4K", models=hlp.models_amipfuture4K)
    plt.savefig("figures/cl_5xdomaintimemean_amip-future4K.pdf")