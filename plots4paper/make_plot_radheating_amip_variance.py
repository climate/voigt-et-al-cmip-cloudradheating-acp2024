#!/usr/bin/env python
# coding: utf-8

# Plot intermodel variance of zonal-mean time-mean radiative heating in all-sky, clear-sky and cloud-radiative heating,
# as well as covariance between clear-sky and cloud radiative heating
# --> show that model spread in all-sky radiative heating is primarily caused by model spread in cloud-radiative heating

# on masterhub of img univie, call as follows:
# /home/swd/manual/nwp/2023.1/bin/python3.10 make_plot_radheating_amip_variance.py

# load libraries etc.
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt 
import matplotlib as mpl
import helpers as hlp
import sys
sys.path.append("../analysis/")
from cmip6radheating import define_targetlevels

import warnings
warnings.simplefilter("ignore") 

# Loads model data into a list of datasets.
csrh = list()
crh  = list()
ta   = list()
tp   = list()

modlist = hlp.models_amip.copy()

# remove EC-EArth3 for which clear-sky radiative heating is much weaker compared to the other models
# this - in some sense artificially - increases the model spread in clear-sky radiative heating
modlist.remove("EC-Earth3")

for mod in modlist:
    if mod in ["HadGEM3-GC31-LL", "HadGEM3-GC31-MM", "UKESM1-0-LL"]:
        _csrh_int, lev = hlp.height2pressure_zg(hlp.load_csrh_zontimmean(mod=mod, exp="amip"), mod=mod, exp="amip")
        _crh_int, lev = hlp.height2pressure_zg(hlp.load_crh_zontimmean(mod=mod, exp="amip"), mod=mod, exp="amip")
        # transform from numpy array to transform to dataarray
        _csrh = xr.DataArray(_csrh_int, name="csrh", dims=("lev", "lat"), 
                            coords={"lev": lev, "lat": hlp.load_csrh_zontimmean(mod=mod, exp="amip").lat})
        _crh = xr.DataArray(_crh_int, name="crh", dims=("lev", "lat"), 
                            coords={"lev": lev, "lat": hlp.load_crh_zontimmean(mod=mod, exp="amip").lat})
        csrh.append(_csrh)
        crh.append(_crh)
    else:
        csrh.append(hlp.load_csrh_zontimmean(mod=mod, exp="amip"))
        crh.append(hlp.load_crh_zontimmean(mod=mod, exp="amip"))
    ta.append(hlp.ta_Amon_ymonmean(mod=mod, exp="amip")["ta"].mean(["month", "lon"]).squeeze())
    tp.append(hlp.tropo_Amon_zontimmean(mod=mod, exp="amip")["trop"].squeeze())

# Interpolate models to same latitude.
latint = np.linspace(-89,89,179)
csrh_latint = list()
for ds in csrh:
    csrh_latint.append(ds.interp(lat=latint).compute())
crh_latint = list()
for ds in crh:
    crh_latint.append(ds.interp(lat=latint).compute())
ta_latint = list()
for ds in ta:
    ta_latint.append(ds.interp(lat=latint).compute())
tp_latint = list()
for ds in tp:
    tp_latint.append(ds.interp(lat=latint).compute())

# Constructs numpy array of multi model ensemble, dimensions of model x lev x latint
csrh_mm = np.array([csrh_latint[i] for i in range(len(csrh_latint))])
crh_mm  = np.array([crh_latint[i] for i in range(len(crh_latint))])
ta_mm   = np.array([ta_latint[i]  for i in range(len(ta_latint)) ])
tp_mm   = np.array([tp_latint[i]  for i in range(len(tp_latint)) ])

# all-sky radiative heating
asrh_mm = csrh_mm+crh_mm

def covariance(data1, data2):
    """
    Computes covariance assuming a numpy array of dimensions (models, lev, lat).
    When data1 == data2, then this gives the variance.
    """
    nmod = np.shape(data1)[0]
    nlev = np.shape(data1)[1]
    nlat = np.shape(data1)[2]
    cov = np.nansum((data1 - np.nanmean(data1, axis=0)) * (data2 - np.nanmean(data2, axis=0)), axis=0)
    # divide by number of available models, take into account that some models have nan in some parts
    # of the atmosphere
    nmod_latlev = np.nansum(0*data1 + 1.0, axis=0)
    cov = cov /nmod_latlev
    return cov

def niceaxes(ax):
    """
    Produces nice x and y axes for plots.
    """
    plt.ylim(1000,10)
    plt.yticks([800,600,400,200],[" ", " ", " ", " "], size=10);
    plt.xlim(-83,83)
    plt.xticks([-60,-30,0,30,60],[" ", " "," "," "," "], size=10, va="top");
    ax.tick_params(direction="in", length=4)

# pressure leves in hPa
plev=1e-2*define_targetlevels()

# levels for contour plots
varlev=np.linspace(0,0.1,11)
covarlev=np.delete(np.linspace(-0.04,0.04,9), 4)

# proceeed with plot
fig, axs = plt.subplots(2, 2, layout="constrained", figsize=(12,8))

ax=plt.subplot(2,2,1)
cnt=plt.contourf(latint, plev, covariance(asrh_mm,asrh_mm), levels=varlev, cmap="gray_r", extend="max")
for c in cnt.collections: c.set_edgecolor("face")
plt.contour(latint, plev, np.nanmedian(ta_mm, axis=0), levels=[273.15-38, 273.15], colors="darkgray", linestyles="-", linewidths=1)
plt.plot(latint, np.nanmedian(tp_mm, axis=0), color="seagreen", linewidth=1)
niceaxes(ax)
plt.ylabel(r"pressure / hPa", size=10, loc="top")
plt.yticks([1000,800,600,400,200,10], ["1000", "800", "600", "400", "200", "10"], fontsize=10)
ax.text(0.03,0.96, "a) All-sky heating", ha="left", va="top", transform=ax.transAxes, backgroundcolor="white", 
        size=12, bbox=dict(facecolor="white", edgecolor="k", boxstyle="round,pad=0.3"))

ax=plt.subplot(2,2,2)
cnt=plt.contourf(latint, plev, covariance(crh_mm,crh_mm), levels=varlev, cmap="gray_r", extend="max")
for c in cnt.collections: c.set_edgecolor("face")
plt.contour(latint, plev, np.nanmedian(ta_mm, axis=0), levels=[273.15-38, 273.15], colors="darkgray", linestyles="-", linewidths=1)
plt.plot(latint, np.nanmedian(tp_mm, axis=0), color="seagreen", linewidth=1)
niceaxes(ax)
ax.text(0.03,0.96, "b) Cloud-rad. heating", ha="left", va="top", transform=ax.transAxes, backgroundcolor="white", 
        size=12, bbox=dict(facecolor="white", edgecolor="k", boxstyle="round,pad=0.3"))
cbar=plt.colorbar(cnt)
cbar.set_ticks([0.0, 0.02, 0.04, 0.06, 0.08, 0.10])
cbar.ax.tick_params(labelsize=10)
cbar.set_label(label=r"K$^2$/day$^2$", size=10)


ax=plt.subplot(2,2,3)
cnt=plt.contourf(latint, plev, covariance(csrh_mm,csrh_mm), levels=varlev, cmap="gray_r", extend="max")
for c in cnt.collections: c.set_edgecolor("face")
plt.contour(latint, plev, np.nanmedian(ta_mm, axis=0), levels=[273.15-38, 273.15], colors="darkgray", linestyles="-", linewidths=1)
plt.plot(latint, np.nanmedian(tp_mm, axis=0), color="seagreen", linewidth=1)
niceaxes(ax)
plt.ylabel(r"pressure / hPa", size=10, loc="top")
plt.yticks([1000,800,600,400,200,10], ["1000", "800", "600", "400", "200", "10"], fontsize=10)
plt.xticks([-60,-30,0,30,60],["60S", "30S","Eq","30N","60N"], size=10, va="top");
ax.text(0.03,0.96, "c) Clear-sky heating", ha="left", va="top", transform=ax.transAxes, backgroundcolor="white", 
        size=12, bbox=dict(facecolor="white", edgecolor="k", boxstyle="round,pad=0.3"))


ax=plt.subplot(2,2,4)
cnt=plt.contourf(latint, plev, 2*covariance(csrh_mm,crh_mm), levels=covarlev, cmap="RdBu_r", extend="both")
for c in cnt.collections: c.set_edgecolor("face")
plt.contour(latint, plev, np.nanmedian(ta_mm, axis=0), levels=[273.15-38, 273.15], colors="darkgray", linestyles="-", linewidths=1)
plt.plot(latint, np.nanmedian(tp_mm, axis=0), color="seagreen", linewidth=1)
niceaxes(ax)
ax.text(0.03,0.96, "d) 2 x Covariance", ha="left", va="top", transform=ax.transAxes, backgroundcolor="white", 
        size=12, bbox=dict(facecolor="white", edgecolor="k", boxstyle="round,pad=0.3"))
niceaxes(ax)
plt.xticks([-60,-30,0,30,60],["60S", "30S","Eq","30N","60N"], size=10, va="top");
plt.xlabel("latitude / deg", loc="right", fontsize=10)
cbar=plt.colorbar(cnt)
cbar.set_ticks(covarlev)
cbar.ax.tick_params(labelsize=10)
cbar.set_label(label=r"K$^2$/day$^2$", size=10)

hlp.spacing_subplots()

plt.savefig("figures/radheating_zonaltimemean_amip_variance.pdf")