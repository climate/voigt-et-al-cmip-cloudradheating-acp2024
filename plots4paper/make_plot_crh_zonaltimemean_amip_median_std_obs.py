#!/usr/bin/env python
# coding: utf-8

# Zonal-mean time-mean CRH for all available models: model median, model spread and comparison to observations including cloud cover

# on srvx1 of img univie, call as follows:
# /home/swd/manual/nwp/2023.1/bin/python3.10 make_plot_crh_zonaltimemean_amip_median_std_obs.py

# load libraries etc.
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt 
import helpers as hlp
import sys
sys.path.append("../analysis/")
from cmip6radheating import define_targetlevels

from cmcrameri import cm as cmc    # nice colorbars for cloud fraction; adapted from Bertrand et al. 2024 colorbar for cloud fraction
                                   # see https://github.com/bertrandclim/essd2023/blob/v1.0/9-9_ESSD-figs-export_R2R_oct26.ipynb

# Loads observational estimates of CRH: FLXHR-LIDAR and CCCM; also cloud cover
cc_r05 = hlp.acre_obs_2bflxhrlidar_r05() # 2b-flxhr-lidar r05
cc_r04 = hlp.acre_obs_2bflxhrlidar_r04() # 2b-flxhr-lidar r04
cccm   = hlp.acre_obs_cccm() # cccm
clobs  = hlp.cc_obs_bertrand2024() # cloud cover from cloudsat/calipso by Bertrand et al. 2024

# Loads model data into a list of datasets.
crh = list()
ta  = list()
tp  = list()

for mod in hlp.models_amip:
    if mod in ["HadGEM3-GC31-LL", "HadGEM3-GC31-MM", "UKESM1-0-LL"]:
        _crh_int, lev = hlp.height2pressure_zg(hlp.load_crh_zontimmean(mod=mod, exp="amip"), mod=mod, exp="amip")
        # _crh_int is a numpy array, need to transform to dataarray
        _crh = xr.DataArray(_crh_int, name="crh", dims=("lev", "lat"), 
                            coords={"lev": lev, "lat": hlp.load_crh_zontimmean(mod=mod, exp="amip").lat})
        crh.append(_crh)
    else:
        crh.append(hlp.load_crh_zontimmean(mod=mod, exp="amip"))
    ta.append(hlp.ta_Amon_ymonmean(mod=mod, exp="amip")["ta"].mean(["month", "lon"]).squeeze())
    tp.append(hlp.tropo_Amon_zontimmean(mod=mod, exp="amip")["trop"].squeeze())

# Interpolates models to same latitude.
latint = np.linspace(-89,89,179)
crh_latint = list()
for ds in crh:
    crh_latint.append(ds.interp(lat=latint).compute())
ta_latint = list()
for ds in ta:
    ta_latint.append(ds.interp(lat=latint).compute())
tp_latint = list()
for ds in tp:
    tp_latint.append(ds.interp(lat=latint).compute())

# Constructs numpy array of multi model ensemble, dimensions of model x lev x latint.m
crh_mm = np.array([crh_latint[i] for i in range(len(crh_latint))])
ta_mm  = np.array([ta_latint[i]  for i in range(len(ta_latint)) ])
tp_mm  = np.array([tp_latint[i]  for i in range(len(tp_latint)) ])

# Plotting.
# 
# Colorbar handling made easy by using fig, axs = plt.subplots(2, 2, layout="constrained", figsize=(12,8)); see https://matplotlib.org/stable/gallery/subplots_axes_and_figures/colorbar_placement.html.
def niceaxes(ax):
    plt.ylim(1000,10)
    plt.yticks([800,600,400,200],[" ", " ", " ", " "], size=10);
    plt.xlim(-80,80)
    plt.xticks([-60,-30,0,30,60],[" ", " ", " ", " ", " "], size=10, va="top");
    ax.tick_params(direction="in", length=4)

lev = crh[0].lev/100 
clev=np.delete(1.0*np.linspace(-1,1,21),10) # contour levels for CRH, remove 0
cl_clev=100*np.linspace(0,1,21)[1:] # [5,15,25,35,45,55,65,75,85,90] # contour levels for cloud cover

fig, axs = plt.subplots(3, 2, layout="constrained", figsize=(12,12))

ax=plt.subplot(3,2,1)
cnt=plt.contourf(latint, lev, np.nanmedian(crh_mm, axis=0), levels=clev, cmap="RdBu_r")
plt.contour(latint, lev, np.nanmedian(ta_mm, axis=0), levels=[273.15-38, 273.15], colors="darkgray", linestyles="-", linewidths=1)
plt.plot(latint, np.nanmedian(tp_mm, axis=0), color="seagreen", linewidth=1)
niceaxes(ax)
ax.text(0.03,0.96, "a) Model median", ha="left", va="top", transform=ax.transAxes, backgroundcolor="white", 
        size=12, bbox=dict(facecolor="white", edgecolor="k", boxstyle="round,pad=0.3"))
plt.ylabel(r"pressure / hPa", size=10, loc="top")
plt.yticks([1000,800,600,400,200,10], ["1000", "800", "600", "400", "200", "10"], fontsize=10)
for c in cnt.collections: c.set_edgecolor("face")

ax=plt.subplot(3,2,2)
cnt=plt.contourf(latint, lev, np.nanstd(crh_mm, axis=0), levels=np.linspace(0,0.5,6), cmap="gray_r", extend="max")
plt.contour(latint, lev, np.nanmedian(ta_mm, axis=0), levels=[273.15-38, 273.15], colors="darkgray", linestyles="-", linewidths=1)
plt.plot(latint, np.nanmedian(tp_mm, axis=0), color="seagreen", linewidth=1)
cbar=plt.colorbar(cnt)
cbar.set_ticks([0,0.1,0.2,0.3,0.4,0.5])
cbar.ax.tick_params(labelsize=10)
cbar.set_label(label="K/day", size=10)
niceaxes(ax)
ax.text(0.03,0.96, "b) Std. deviation\n    across models", ha="left", va="top", transform=ax.transAxes, backgroundcolor="white", 
        size=12, bbox=dict(facecolor="white", edgecolor="k", boxstyle="round,pad=0.3"))
for c in cnt.collections: c.set_edgecolor("face")

ax=plt.subplot(3,2,3)
cnt=plt.contourf(cc_r05.lat, cc_r05.lev/100, cc_r05, levels=clev, cmap="RdBu_r", extend="both")
plt.contour(latint, lev, np.nanmedian(ta_mm, axis=0), levels=[273.15-38, 273.15], colors="darkgray", linestyles="-", linewidths=1)
plt.plot(latint, np.nanmedian(tp_mm, axis=0), color="seagreen", linewidth=1)
niceaxes(ax)
ax.text(0.03,0.96, "c) 2B-FLXHR-LIDAR R05", ha="left", va="top", transform=ax.transAxes, backgroundcolor="white", 
        size=12, bbox=dict(facecolor="white", edgecolor="k", boxstyle="round,pad=0.3"))
plt.ylabel(r"pressure / hPa", size=10, loc="top")
plt.yticks([1000,800,600,400,200,10], ["1000", "800", "600", "400", "200", "10"], fontsize=10)
for c in cnt.collections: c.set_edgecolor("face")

ax=plt.subplot(3,2,4)
cnt=plt.contourf(cc_r04.lat, cc_r04.lev/100, cc_r04, levels=clev, cmap="RdBu_r", extend="both")
plt.contour(latint, lev, np.nanmedian(ta_mm, axis=0), levels=[273.15-38, 273.15], colors="darkgray", linestyles="-", linewidths=1)
plt.plot(latint, np.nanmedian(tp_mm, axis=0), color="seagreen", linewidth=1)
niceaxes(ax)
cbar=plt.colorbar(cnt)
cbar.set_ticks([-1, -0.7, -0.4, 0.0, 0.4, 0.7, 1.0])
cbar.ax.tick_params(labelsize=10)
cbar.set_label(label="K/day", size=10)
ax.text(0.03,0.96, "d) 2B-FLXHR-LIDAR R04", ha="left", va="top", transform=ax.transAxes, backgroundcolor="white", 
        size=12, bbox=dict(facecolor="white", edgecolor="k", boxstyle="round,pad=0.3"))
for c in cnt.collections: c.set_edgecolor("face")

ax=plt.subplot(3,2,5)
cnt=plt.contourf(cccm.lat, cccm.lev/100, cccm, levels=clev, cmap="RdBu_r", extend="both")
plt.contour(latint, lev, np.nanmedian(ta_mm, axis=0), levels=[273.15-38, 273.15], colors="darkgray", linestyles="-", linewidths=1)
plt.plot(latint, np.nanmedian(tp_mm, axis=0), color="seagreen", linewidth=1)
niceaxes(ax)
ax.text(0.03,0.96, "e) CCCM", ha="left", va="top", transform=ax.transAxes, backgroundcolor="white", 
        size=12, bbox=dict(facecolor="white", edgecolor="k", boxstyle="round,pad=0.3"))
plt.xlabel("latitude / deg", size=10, loc="right")
plt.xticks([-60,-30,0,30,60],["60S", "30S","Eq","30N","60N"], size=10, va="top");
plt.ylabel(r"pressure / hPa", size=10, loc="top")
plt.yticks([1000,800,600,400,200,10], ["1000", "800", "600", "400", "200", "10"], fontsize=10)
for c in cnt.collections: c.set_edgecolor("face")

ax=plt.subplot(3,2,6)
cnt=plt.contourf(clobs.lat, clobs.lev/100, clobs, levels=np.arange(0.,0.7,0.05), cmap=cmc.batlow)
for c in cnt.collections: c.set_edgecolor("face")
plt.contour(latint, lev, np.nanmedian(ta_mm, axis=0), levels=[273.15-38, 273.15], colors="darkgray", linestyles="-", linewidths=1)
plt.plot(latint, np.nanmedian(tp_mm, axis=0), color="white", linewidth=1)
niceaxes(ax)
cbar=plt.colorbar(cnt)
cbar.set_ticks([0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6])
cbar.ax.tick_params(labelsize=10)
cbar.set_label(label=" ", size=10)
ax.text(0.03,0.96, "f) Observed cloud fraction", ha="left", va="top", transform=ax.transAxes, backgroundcolor="white", 
        size=12, bbox=dict(facecolor="white", edgecolor="k", boxstyle="round,pad=0.3"))
plt.xlabel("latitude / deg", size=10, loc="right")
plt.xticks([-60,-30,0,30,60],["60S", "30S","Eq","30N","60N"], size=10, va="top");

plt.savefig("figures/crh_zonaltimemean_amip_median_std_obs.pdf")



