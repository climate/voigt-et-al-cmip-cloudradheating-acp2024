#!/usr/bin/env python
# coding: utf-8

# Plots of CRH sampled with T as vertical coordinate for all available models and the amip, amip-p4K and amip-future4K simulations
# Generates the following types of multi-panel plots of CRH:
#   * Plot 1: one panel per model, using only one CRH approach per model;
#   * Plot 2: vertical profiles of CRH averaged over 5 domains

# on srvx1 of img univie, call as follows:
# /home/swd/manual/nwp/2023.1/bin/python3.10 make_plots_crh_Tlev.py

import os
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt 
import matplotlib as mpl
import helpers as hlp

#----------------------------------------------------------------------
# Defines switches re which plots should be generated
#----------------------------------------------------------------------
lplot1=True
lplot2=True

#----------------------------------------------------------------------
# Defines plotting functions
#----------------------------------------------------------------------

# Plot 1:
# zonal-mean time-mean CRH for all available models of a given simulation in a 4-column panel plot
def plot_crh_Tlev_xypanelplot(exp, models):
    
    # temperature levels
    Tint=np.linspace(100,320,221)
    
    crh_clev=np.delete(1.0*np.linspace(-1,1,21),10) # contour levels for CRH, remove 0
    nmod = len(models) # number of models
    
    ncol=4
    # number of panel rows as a function of number of models, we add one extra row to place colorbar there
    nrow=int(np.floor(nmod/ncol)) + 1
    if np.mod(nmod,ncol) > 0: nrow+=1

    def _plot_crh_singledata(crh, ax):
        """ Plots zonal-mean time-mean CRH for T as vertical coordinate."""
        cnt=plt.contourf(crh.lat, crh.Tint, crh, crh_clev, cmap="RdBu_r", extend="both")
        for c in cnt.collections: c.set_edgecolor("face")
        plt.ylim(300,180)
        plt.yticks([300,270,240,210,180],[" ", " ", " ", " ", " "], size=12);
        plt.xlim(-88,88)
        plt.xticks([-60,-30,0,30,60],[" ", " ", " ", " ", " "], size=12, va="top");
        ax.tick_params(direction="in", length=6)
        return
   
    # we use one extra row to place the colorbar there
    fig=plt.figure(figsize=(4*6,nrow*4))
    
    # loop over models, use crh in following order: 1) from CFmon flux, 2) from tntr of CFmon, AERmon, Emon, 3) from tntr of EmonZ
    counter=0
    for mod in models:
        counter+=1
        crh = hlp.crh_zonaltimemean_Tlevels(mod, exp=exp, Tint=Tint)
        ax=plt.subplot(nrow, ncol, counter)
        _plot_crh_singledata(crh, ax)
        if np.mod(counter, ncol) == 1:
            plt.ylabel("temperature / K", loc="top", fontsize=15)
            plt.yticks([300,270,240,210,180],["300","270","240","210","180"], size=12);
        if counter>ncol*(nrow-2):
            plt.xlabel("latitude / deg", loc="right", fontsize=15)
            plt.xticks([-60,-30,0,30,60],["60S", "30S","Eq","30N","60N"], size=12, va="top");
        # for rows that are not completely filled, we need to include
        # xlabel in the panels of the row above
        if counter+ncol>nmod: 
            plt.xlabel("latitude / deg", loc="right", fontsize=15)
            plt.xticks([-60,-30,0,30,60],["60S", "30S","Eq","30N","60N"], size=12, va="top");
        ax.text(0.03,0.96, mod, ha="left", va="top", transform=ax.transAxes, backgroundcolor="white", 
                size=15, bbox=dict(facecolor="white", edgecolor="k", boxstyle="round,pad=0.3"))       
   
    # plot colorbar
    # solution taken and adapted from https://stackoverflow.com/a/62436015
    # axes position found manually, to do so use "print(ax)" for above subplots
    if exp=="amip-p4K" or exp=="amip-future4K":
        ax = fig.add_axes([0.714,0.381765,0.185, 0.04/nrow])
    if exp=="amip":
        ax = fig.add_axes([0.714,0.20,0.185, 0.04/nrow])
    cb = mpl.colorbar.ColorbarBase(ax, orientation="horizontal", 
                                   cmap="RdBu_r", extend="both", ticks=[-1, -0.7, -0.4, 0.0, 0.4, 0.7, 1.0],
                                   label="K/day",
                                   boundaries=crh_clev, norm=mpl.colors.Normalize(crh_clev[0], crh_clev[-1]))     
    cb.ax.tick_params(labelsize=15)
    cb.set_label(label="K/day", size=15)
    
    hlp.spacing_subplots()

    
# Plot 2:
# 5 panel plot for 5 domain averages ordered from South to North
def plot_crh_Tlev_5domains(exp, models):
    
    # temperature levels
    Tint=np.linspace(100,320,221)
    
    nmod = len(models) # number of models
    
    def _make_niceaxes(ax, ticklength=0.08):
        # adjust spines
        ax.spines["top"].set_color("none")
        ax.spines["right"].set_color("none")
        ax.xaxis.set_ticks_position("bottom")
        ax.spines["bottom"].set_position(("data",300))
        ax.spines["left"].set_position(("data",-0.6))
        ax.spines["left"].set_color("none")
        plt.ylim(300,180)
        plt.yticks([ ], fontsize=10) 
        plt.plot([0,0], [300,180], linewidth=0.7, color="k", zorder=-10)
        for ypos in [300,270,240,210,180]:
            plt.plot([-0.5*ticklength,0.5*ticklength], [ypos,ypos], linewidth=0.7, color="k", zorder=-10)
    
    def _plot_crh_singledata(_crh, mod):
        plt.plot(_crh, _crh.Tint, color="gray", label=mod)
        return
    
    # 5 panel plot for 5 domain averages ordered from South to North
    fig=plt.figure(figsize=(16,4))
    for mod in models:
        crh = hlp.crh_zonaltimemean_Tlevels(mod, exp=exp, Tint=Tint)
        # 70S - 35S
        ax=plt.subplot(1,5,1)     
        _plot_crh_singledata(hlp.compute_domainmean(crh, lats=-70, latn=-35), mod=mod)
        # 35S - 15S
        ax=plt.subplot(1,5,2)     
        _plot_crh_singledata(hlp.compute_domainmean(crh, lats=-35, latn=-15), mod=mod)
        # 15S - 15N
        ax=plt.subplot(1,5,3)     
        _plot_crh_singledata(hlp.compute_domainmean(crh, lats=-15, latn=+15), mod=mod)
        # 15N - 35N
        ax=plt.subplot(1,5,4)     
        _plot_crh_singledata(hlp.compute_domainmean(crh, lats=+15, latn=+35), mod=mod)
        # 35N - 70N
        ax=plt.subplot(1,5,5)     
        _plot_crh_singledata(hlp.compute_domainmean(crh, lats=+35, latn=+70), mod=mod)
    
    # make nice axes for subplots
    ax=plt.subplot(1,5,1); _make_niceaxes(ax); plt.title("70S - 35S", fontsize=15, fontweight="bold")
    plt.ylabel(r"temperature / K", size=12, loc="top")
    plt.yticks([300,270,240,210,180], ["300","270","240","210","180"], fontsize=10) 
    ax=plt.subplot(1,5,2); _make_niceaxes(ax); plt.title("35S - 15S", fontsize=15, fontweight="bold")
    ax=plt.subplot(1,5,3); _make_niceaxes(ax); plt.title("15S - 15N", fontsize=15, fontweight="bold"); 
    ax=plt.subplot(1,5,4); _make_niceaxes(ax); plt.title("15N - 35N", fontsize=15, fontweight="bold")
    ax=plt.subplot(1,5,5); _make_niceaxes(ax); plt.title("35N - 70N", fontsize=15, fontweight="bold")
    plt.xlabel(r"cloud-radiative heating / K$\,$day$^{-1}$", size=12, loc="right")
    
    plt.subplots_adjust(left=0.1, bottom=0.15, right=0.9, top=0.9,
                        wspace=0.08, hspace=0.1)

#----------------------------------------------------------------------
# Generates plots by calling plotting functions.
#----------------------------------------------------------------------

# xypanel plots of zonal means
if lplot1:
    # amip
    plot_crh_Tlev_xypanelplot(exp="amip", models=hlp.models_amip)
    plt.savefig("./figures/crh_Tlev_zonaltimemean_xypanelplot_amip.pdf")
    # amip plus 4K SST
    plot_crh_Tlev_xypanelplot(exp="amip-p4K", models=hlp.models_amipp4K)
    plt.savefig("./figures/crh_Tlev_zonaltimemean_xypanelplot_amipp4K.pdf")
    # amip with 4K-future SST increase
    plot_crh_Tlev_xypanelplot(exp="amip-future4K", models=hlp.models_amipfuture4K)
    plt.savefig("./figures/crh_Tlev_zonaltimemean_xypanelplot_amipfuture4K.pdf")
    
# domain averages
if lplot2:
    # amip
    plot_crh_Tlev_5domains(exp="amip", models=hlp.models_amip)
    plt.savefig("figures/crh_Tlev_5xdomaintimemean_amip.pdf")
    # amip-p4K
    plot_crh_Tlev_5domains(exp="amip-p4K", models=hlp.models_amipp4K)
    plt.savefig("figures/crh_Tlev_5xdomaintimemean_amip-p4K.pdf")
    # amip with 4K-future SST increase
    plot_crh_Tlev_5domains(exp="amip-future4K", models=hlp.models_amipfuture4K)
    plt.savefig("figures/crh_Tlev_5xdomaintimemean_amip-future4K.pdf")