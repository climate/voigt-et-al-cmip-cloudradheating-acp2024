# Git repository for "Atmospheric cloud-radiative heating in CMIP6 and observations, and its response to surface warming" by Aiko Voigt et al.

For the EGU ACP paper accepted in July 2024.

**Author:** Aiko Voigt, IMG, University of Vienna, aiko.voigt@univie.ac.at.

**The repository contains the following directories:**

 * acp: manuscript files for accepted version but before technical changes for production, allows to link the figure file names to the plotting scripts in the directory plots4paper
 * analysis: scripts for postprocessing the data downloaded from ESGF, including the calculation of radiative heating rates, the thermal tropopause, and time and zonal means; note that geopotential height zg for UKESM and HadGEM models is directly put into the directory "mean" by the postprocessing script
 * cfmip-sst: netcdf file with SST pattern change used in the CMIP6 amip-future4K simulations
 * data-download: scripts for retrieving model output from ESGF
 * obsdata: zonal-mean time-mean estimates of cloud-radiative heating from CCCM and 2B-FLXHR-LIDAR
 * plots4paper: scripts to generate plots used in the paper, the plots are stored in the subdirectory "figures", from where they are accesses in the latex file; note: the directory contains also figures that were not included in the paper

The repo is a (subset) copy from the GitLab repo https://gitlab.phaidra.org/climate/cmip6-crh, commit 4b9920be928f3ac00e3ee9da0ae88f9522adb19a. The cmip6-crh repo is internal to the team "Climate Dynamics and Modeling" at IMG, University of Vienna.
 
 The directory paths will need to be adapated by the user to their own local infrastructure. This should be relatively straightforward.
 
 The postprocessing scripts for the cloud-radiative heating from CCCM and 2B-FLXHR-LIDAR are provided in a separate repository. Likewise, the CCCM/2B-FLXHR-LIDAR postprocessed data and the CMIP6 postprocessed data (mostly 12-month climatologies) are provided in separate repositories that are given in the data statement of the ACP paper.
 
Good luck and enjoy! 
