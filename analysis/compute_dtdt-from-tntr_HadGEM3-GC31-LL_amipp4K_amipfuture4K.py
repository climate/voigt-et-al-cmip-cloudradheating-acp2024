#!/usr/bin/env python
# coding: utf-8
#
# on IMG SRVX1, call the python script as:
# /home/swd/manual/nwp/2023.1/bin/python3.10 compute_dtdt-from-tntr_HadGEM3-GC31-LL_amipp4K_amipfuture4K.py

# # Postprocess heating rates for HadGEM3-GC31-LL model, which is on height levels,
# for amip-p4K and amip-future4K simulations
#
# Special treatment is necessary because for this model and these simulations, the following data
# needs to be used:
# - tntr from CFmon
# - tntrlcs and tntrscs from Emon

import sys
sys.path.append("/users/staff/avoigt/cmip-acre/analysis")
import cmip6radheating

import numpy as np
import xarray as xr
import zarr


# Defines function that contains all steps to compute radiative heating rate from tntr variables.
def compute_dtdt_from_tntr(_ds_list, _var_list, _varout):
    """ Computes heating rates for a list of one or several data sets
    that each contain a radiative heating rate. The net radiative 
    heating rate is calculated as the sum of the input radiative heating rates.
    
    _ds_list: list of radiative heating rate datasets
    _var_list: list of the variable names of the radiative heating rates in _ds_list
    """
    import shutil    
    _model = _ds_list[0].attrs["source_id"]
    # compute net radiative heating rate from the sum of input radiative heating rates
    _dtdt = cmip6radheating.add_heatingrate(_ds_list, _var_list)
    # height levels for interpolation
    _hgtint = np.arange(0,20e3,2e2)
    _dtdt_int = cmip6radheating.interpolate2height(_dtdt, ["dTdt"], _hgtint).rename({'dTdt': _varout})
    # write interpolated heating rate to zarr store
    _path = "/scratch/das/avoigt/cmip6-acre-data/postprocessed/"
    _mod = _dtdt_int.attrs["source_id"]
    _exp = _dtdt_int.attrs["experiment_id"]
    _tab = _dtdt_int.attrs["table_id"]
    _out = _path+_varout+"_"+_tab+"_"+_mod+"_"+_exp+".zarr"
    try:
        shutil.rmtree(_out, ignore_errors=False, onerror=None)
    except:
        pass
    _dtdt_int.to_zarr(_out)


mod="HadGEM3-GC31-LL"
    
# Loops over experiments, tables and the two models.
for exp in ["amip-p4K", "amip-future4K"]:
    # all-sky radiative heating
    tab="CFmon"
    print("Working on all-sky radiative heating", exp, tab, mod)
    flist_tntr = cmip6radheating.filelist_allsky_tntronly(exp=exp, tab=tab, mod=mod)
    ds_tntr = (cmip6radheating.load_data(flist_tntr)[0])
    compute_dtdt_from_tntr([ds_tntr], ["tntr"], "dTdt-as-from-tntr")
    print("Finished all-sky computation for", exp, tab, mod)
    # clear-sky radiative heating
    tab="Emon"
    print("Working on clr-sky radiative heating", exp, tab, mod)
    flist_tntrscs, flist_tntrlcs = cmip6radheating.filelist_clrsky_tntr(exp=exp, tab=tab, mod=mod)
    if len(flist_tntrscs)>0 and len(flist_tntrlcs)>0:
        ds_tntrscs = (cmip6radheating.load_data(flist_tntrscs)[0])
        ds_tntrlcs = (cmip6radheating.load_data(flist_tntrlcs)[0])
        compute_dtdt_from_tntr([ds_tntrscs, ds_tntrlcs], ["tntrscs", "tntrlcs"], 
                                "dTdt-cs-from-tntr")
        compute_dtdt_from_tntr([ds_tntrscs], ["tntrscs"], "dTdts-cs-from-tntr")
        compute_dtdt_from_tntr([ds_tntrlcs], ["tntrlcs"], "dTdtl-cs-from-tntr")
    print("Finished clr-sky computation for", exp, tab, mod)