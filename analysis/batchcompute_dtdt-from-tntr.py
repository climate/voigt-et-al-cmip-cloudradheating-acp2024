#!/usr/bin/env python
# coding: utf-8
#
# on srvx1 call as
# /home/swd/manual/nwp/2023.1/bin/python3.10 batchcompute_dtdt-from-tntr.py amip AERmon

import warnings
warnings.simplefilter("ignore") 

import sys
args = sys.argv
# args[0]: name of the script
# args[1]: experiment, first argument passed in
# args[2]: table, second argument passed in
# etc.
exp=args[1]
tab=args[2]

import cmip6radheating
from multiprocessing import Process

flist_tntrs, flist_tntrl = cmip6radheating.filelist_allsky_tntr(exp=exp, tab=tab)
ds_tntrs = cmip6radheating.load_data(flist_tntrs)
ds_tntrl = cmip6radheating.load_data(flist_tntrl)

flist_tntrscs, flist_tntrlcs = cmip6radheating.filelist_clrsky_tntr(exp=exp, tab=tab)
ds_tntrscs = cmip6radheating.load_data(flist_tntrscs)
ds_tntrlcs = cmip6radheating.load_data(flist_tntrlcs)

# function that computes radiative heating rates
# below we loop over the function using multiprocessing
def compute_dtdt_from_tntr(_ds_list, _var_list, _varout):
    """ Computes heating rates for a list of one or several data sets
    that each contain a radiative heating rate. The net radiative 
    heating rate is calculated as the sum of the input radiative heating rates.
    
    _ds_list: list of radiative heating rate datasets
    _var_list: list of the variable names of the radiative heating rates in _ds_list
    """
    import shutil    
    _model = _ds_list[0].attrs["source_id"]
    # compute net radiative heating rate from the sum of input radiative heating rates
    _dtdt = cmip6radheating.add_heatingrate(_ds_list, _var_list)
    try:
        _dtdt = cmip6radheating.addpressure2dataset(_dtdt, model=_model, levtype="full")
        _levint = cmip6radheating.define_targetlevels()
        _dtdt_int = cmip6radheating.interpolate2pressure(_dtdt, ["dTdt"], _levint).rename({'dTdt': _varout})
        # write interpolated heating rate to zarr store
        _path = "/scratch/das/avoigt/cmip6-acre-data/postprocessed/"
        _mod = _dtdt_int.attrs["source_id"]
        _exp = _dtdt_int.attrs["experiment_id"]
        _tab = _dtdt_int.attrs["table_id"]
        _out = _path+_varout+"_"+_tab+"_"+_mod+"_"+_exp+".zarr"
        try:
            shutil.rmtree(_out, ignore_errors=False, onerror=None)
        except:
            pass
        _dtdt_int.to_zarr(_out)
        print(_model, "finished")
    except:
        print(_model, "cannot be calculated")

        
# all-sky radiative heating rate
processes = [Process(target=compute_dtdt_from_tntr, 
                     args=([ds_tntrs[i], ds_tntrl[i]], 
                           ["tntrs", "tntrl"], "dTdt-as-from-tntr"),) for i in range(0, len(ds_tntrs))]
for process in processes: process.start() # start all processes
for process in processes: process.join()  # wait for all processes to complete

processes = [Process(target=compute_dtdt_from_tntr, 
                     args=([ds_tntrs[i]], 
                           ["tntrs"], "dTdts-as-from-tntr"),) for i in range(0, len(ds_tntrs))]
for process in processes: process.start() # start all processes
for process in processes: process.join()  # wait for all processes to complete

processes = [Process(target=compute_dtdt_from_tntr, 
                     args=([ds_tntrl[i]], 
                           ["tntrl"], "dTdtl-as-from-tntr"),) for i in range(0, len(ds_tntrs))]
for process in processes: process.start() # start all processes
for process in processes: process.join()  # wait for all processes to complete

# clear-sky radiative heating rate
processes = [Process(target=compute_dtdt_from_tntr, 
                     args=([ds_tntrscs[i], ds_tntrlcs[i]], 
                           ["tntrscs", "tntrlcs"], "dTdt-cs-from-tntr"),) for i in range(0, len(ds_tntrscs))]
for process in processes: process.start() # start all processes
for process in processes: process.join()  # wait for all processes to complete

processes = [Process(target=compute_dtdt_from_tntr, 
                     args=([ds_tntrscs[i]], 
                           ["tntrscs"], "dTdts-cs-from-tntr"),) for i in range(0, len(ds_tntrscs))]
for process in processes: process.start() # start all processes
for process in processes: process.join()  # wait for all processes to complete

processes = [Process(target=compute_dtdt_from_tntr, 
                     args=([ds_tntrlcs[i]], 
                           ["tntrlcs"], "dTdtl-cs-from-tntr"),) for i in range(0, len(ds_tntrscs))]
for process in processes: process.start() # start all processes
for process in processes: process.join()  # wait for all processes to complete