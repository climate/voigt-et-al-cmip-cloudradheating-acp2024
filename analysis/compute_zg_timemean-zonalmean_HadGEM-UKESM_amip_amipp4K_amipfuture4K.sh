#!/bin/bash

# Compute climatology of time-mean zonal-mean zg from Amon table for HadGEM and UKESM models for
# amip, amip-p4K and amip-future4K simulations an
# on srvx1 of IMG UNIVIE call as
# ./compute_zg_timemean-zonalmean_HadGEM-UKESM_amip_amipp4K_amipfuture4K.sh

module load cdo

datadir="/scratch/das/avoigt/cmip6-acre-data/CMIP6/"
outdir="/scratch/das/avoigt/cmip6-acre-data/postprocessed/mean/"
scriptdir="/users/staff/avoigt/cmip6-crh/analysis"

echo "Moving into data directory"
cd $datadir

echo "1st step: working on amip"
cd amip/zg/Amon

cdo -zonmean -timmean -yearmonmean -selyear,1980/2014 zg_Amon_HadGEM3-GC31-LL_amip_r1i1p1f3_gn_197901-201412.nc ${outdir}/zg_Amon_HadGEM3-GC31-LL_amip.timmean.zonmean.nc
cdo -zonmean -timmean -yearmonmean -selyear,1980/2014 -mergetime zg_Amon_HadGEM3-GC31-MM_amip_r1i1p1f3_gn_197901-198912.nc zg_Amon_HadGEM3-GC31-MM_amip_r1i1p1f3_gn_199001-200912.nc zg_Amon_HadGEM3-GC31-MM_amip_r1i1p1f3_gn_201001-201412.nc ${outdir}/zg_Amon_HadGEM3-GC31-MM_amip.timmean.zonmean.nc
cdo -zonmean -timmean -yearmonmean -selyear,1980/2014 zg_Amon_UKESM1-0-LL_amip_r1i1p1f4_gn_197901-201412.nc ${outdir}/zg_Amon_UKESM1-0-LL_amip.timmean.zonmean.nc

echo "2nd step: working on amip-p4K"
cd ${datadir}/amip-p4K/zg/Amon
cdo -zonmean -timmean -yearmonmean -selyear,1980/2014 -mergetime zg_Amon_HadGEM3-GC31-LL_amip-p4K_r5i1p1f3_gn_197901-201412.nc ${outdir}/zg_Amon_HadGEM3-GC31-LL_amip-p4K.timmean.zonmean.nc

echo "3rd step: working on amip-future4K"
cd ${datadir}/amip-future4K/zg/Amon
cdo -zonmean -timmean -yearmonmean -selyear,1980/2014 -mergetime zg_Amon_HadGEM3-GC31-LL_amip-future4K_r5i1p1f3_gn_197901-201412.nc ${outdir}/zg_Amon_HadGEM3-GC31-LL_amip-future4K.timmean.zonmean.nc

echo "Moving back to script directory"
cd $scriptdir
