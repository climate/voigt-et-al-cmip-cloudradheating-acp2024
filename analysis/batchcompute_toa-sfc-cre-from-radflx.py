#!/usr/bin/env python
# coding: utf-8
#
# compute toa and sfc cloud-radiative effects from 3d radiative flux fields
# data is always from CFmon tab
# we consider exp amip, amip-p4K and amip-future4K
#
# on srvx1 call as
# /home/swd/manual/nwp/2023.1/bin/python3.10 batchcompute_toa-sfc-cre-from-radflx.py

import numpy as np
import cmip6radheating
from multiprocessing import Process

# below we loop over the function using multiprocessing
def compute_toa_sfc_radflux_from_flx(_ds_list, _var_list, _sign_list, _varout):
    """ Computes TOA and SFC radiative fluxes for a list of one or several data sets
    that each contain a radiative flux. The net radiative 
    flux is calculated as the sum or difference of the input radiative fluxes.
    
    _ds_list: list of radiative flux datasets
    _var_list: list of the variable names of the radiative fluxes in _ds_list
    _sign_list: list of +-1 that defines whether a flux needs to be added or subtracted
    """
    import shutil    
    _model = _ds_list[0].attrs["source_id"]
    # compute net radiative flux from the sum of input radiative fluxes
    _ds_flx = cmip6radheating.add_radflx(_ds_list, _var_list, _sign_list).rename({"radflx": _varout})
    # figure out, which level is toa and which is sfc 
    _ilev_sfc = np.argmax(_ds_flx.lev.values)
    _ilev_toa = np.argmin(_ds_flx.lev.values)
    # manual treatment of _ilev_sfc and _ile_toa for models IPSL-CM6A-LR, HadGEM3-GC31-LL, HadGEM3-GC31-MM and UKESM1-0-LL
    if _model == "IPSL-CM6A-LR": # lev is an index from 1 .. 79
        _ilev_sfc = np.argmin(_ds_flx.lev.values)
        _ilev_toa = np.argmax(_ds_flx.lev.values)
    if _model == "HadGEM3-GC31-LL" or _model == "HadGEM3-GC31-MM" or _model == "UKESM1-0-LL": # lev corresponds to height above surface
        _ilev_sfc = np.argmin(_ds_flx.lev.values)
        _ilev_toa = np.argmax(_ds_flx.lev.values)
    # write toa and sfc flux to zarr store
    _path = "/scratch/das/avoigt/cmip6-acre-data/postprocessed/"
    _mod = _ds_flx.attrs["source_id"]
    _exp = _ds_flx.attrs["experiment_id"]
    _tab = _ds_flx.attrs["table_id"]
    # toa
    _out = _path+_varout+"t_"+_tab+"_"+_mod+"_"+_exp+".nc"
    try:
        shutil.rmtree(_out, ignore_errors=False, onerror=None)
    except:
        pass
    _ds_flx[_varout].isel(lev=_ilev_toa).rename(_varout+"t").to_netcdf(_out, mode="w")
    # sfc
    _out = _path+_varout+"s_"+_tab+"_"+_mod+"_"+_exp+".nc"
    try:
        shutil.rmtree(_out, ignore_errors=False, onerror=None)
    except:
        pass
    _ds_flx[_varout].isel(lev=_ilev_sfc).rename(_varout+"s").to_netcdf(_out, mode="w")    
    print(_model, "finished")
    return

tab="CFmon"

# loop over experiments
for exp in["amip", "amip-p4K", "amip-future4K"]:
    
    flist_rsd, flist_rsu, flist_rld, flist_rlu = cmip6radheating.filelist_allsky_radfluxes(exp=exp, tab=tab)
    ds_rsd = cmip6radheating.load_data(flist_rsd)
    ds_rsu = cmip6radheating.load_data(flist_rsu)
    ds_rld = cmip6radheating.load_data(flist_rld)
    ds_rlu = cmip6radheating.load_data(flist_rlu)

    flist_rsdcs, flist_rsucs, flist_rldcs, flist_rlucs = cmip6radheating.filelist_clrsky_radfluxes(exp=exp, tab=tab)
    ds_rsdcs = cmip6radheating.load_data(flist_rsdcs)
    ds_rsucs = cmip6radheating.load_data(flist_rsucs)
    ds_rldcs = cmip6radheating.load_data(flist_rldcs)
    ds_rlucs = cmip6radheating.load_data(flist_rlucs)

    # all-sky fluxes
    processes = [Process(target=compute_toa_sfc_radflux_from_flx, 
                     args=([ds_rsd[i], ds_rsu[i]], ["rsd"  , "rsu"], [+1, -1], "rsn"),) for i in range(0, len(ds_rsd))]
    for process in processes: process.start()
    for process in processes: process.join()
    processes = [Process(target=compute_toa_sfc_radflux_from_flx, 
                     args=([ds_rld[i], ds_rlu[i]], ["rld"  , "rlu"], [+1, -1], "rln"),) for i in range(0, len(ds_rld))]
    for process in processes: process.start()
    for process in processes: process.join()
 
    # clear-sky fluxes
    processes = [Process(target=compute_toa_sfc_radflux_from_flx, 
                     args=([ds_rsdcs[i], ds_rsucs[i]], ["rsdcs"  , "rsucs"], [+1, -1], "rsncs"),) for i in range(0, len(ds_rsdcs))]
    for process in processes: process.start()
    for process in processes: process.join()
    processes = [Process(target=compute_toa_sfc_radflux_from_flx, 
                     args=([ds_rldcs[i], ds_rlucs[i]], ["rldcs"  , "rlucs"], [+1, -1], "rlncs"),) for i in range(0, len(ds_rldcs))]
    for process in processes: process.start()
    for process in processes: process.join()