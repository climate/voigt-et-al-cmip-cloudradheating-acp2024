# target pressure levels

def define_targetlevels():
    """ Defines the pressure levels for the interpolation. """
    import numpy as _np
    return _np.arange(1000e2, 0, -10e2)
    
#-----------------------------------------------
    
def define_targetlevels_hgt():
    """ Defines the height levels for the interpolation. """
    import numpy as _np
    return _np.arange(0,20e3,2e2)
    
#-----------------------------------------------

def addpressure2dataset(ds, model, levtype="half"):
    """ Adds atmospheric pressure to the dataset for a given model. Pressure can be
    for full levels or half levels, depending on levtype.
    
    Pressure is calculated based on surface pressure and model dependent parameters
    of the vertical grid that are read from a separate netcdf file. 
    
    The default is to add the pressure of half levels (i.e., for radiative fluexes). 
    To add pressure on full levels (i.e., for temperature and heating rates),
    levtype needs to be set to "full". """
    
    import numpy as np
    import xarray as xr
    
    if model in ["UKESM1-0-LL", "HadGEM3-GC31-LL","HadGEM3-GC31-MM"]: # height based models
        print("WARNING in addpressure2dataset: height based model, treated differently, air pressure not added", 
              "model:", model, flush=True)
        return 
    
    path = "/users/staff/avoigt/cmip6-crh/analysis/vgrid_information/"
    
    if levtype == "half":
        ds_vgrid = xr.load_dataset(path+"/"+"vgrid_halflevels_"+model+".nc")
    elif levtype =="full":
        ds_vgrid = xr.load_dataset(path+"/"+"vgrid_fulllevels_"+model+".nc")
    else:
        print("WARNING in addpressure2dataset: levtype", levtype, "unknown, air pressure not added", flush=True)
        
    if model in ["CNRM-CM6-1", "CNRM-ESM2-1", "GFDL-AM4", "GFDL-CM4", 
                 "IPSL-CM6A-LR", "MPI-ESM1-2-HR", "MPI-ESM1-2-LR", "MPI-ESM-1-2-HAM"]:
        pres = ds_vgrid["ap"] + ds["ps"]*ds_vgrid["b"]       
    elif model in ["BCC-CSM2-MR", "BCC-ESM1", "CESM2", "CESM2-FV2", "CESM2-WACCM", "CESM2-WACCM-FV2", "MIROC-ES2L", "MIROC6", "MRI-ESM2-0"]:
        pres = ds_vgrid["a"]*ds_vgrid["p0"] + ds["ps"]*ds_vgrid["b"]
    elif model in ["INM-CM4-8", "INM-CM5-0"]:
        pres = ds_vgrid["ptop"] + ds_vgrid["lev"]*(ds["ps"] - ds_vgrid["ptop"])
    else:
        print("WARNING in addpressure2dataset: model", model, "unknown, air pressure not added", flush=True)
    
    # add pressure to input dataset
    pres = pres.transpose("time", "lev", "lat", "lon").values
    ds["pres"] = xr.DataArray(pres, name="pres", dims=("time", "lev", "lat", "lon"), 
                                  coords={"time": ds.time, "lev": ds.lev, "lat": ds.lat, "lon": ds.lon})
    
    return ds
        
#-----------------------------------------------

def interpolate2pressure(ds, varnames, lev_int):
    """Interpolate data onto pressure levels using numpy interp.
    
    lev_int is the target pressure levels in Pa."""

    import xarray as xr
    import numpy as np
    
    # dimensions of input data
    ntim = ds.time.size
    nlat = ds.lat.size
    nlon = ds.lon.size

    # output dataset with vertically interpolated 3d fields
    ds_int = xr.Dataset()
    ds_int.attrs = ds.attrs
 
    # compute datset once and load to local dataset
    # this seems to increase performance, but I assume only matters when we use chunked arrays
    _ds = ds.compute()

    # loop over requested variables
    for varname in varnames:
        var  = _ds[varname].values
        lev  = _ds["pres"].values
        var_int = np.zeros((ntim, lev_int.size, nlat, nlon)) + np.nan
        for t in range(ntim):
            for j in range(nlat):
                for i in range(nlon):
                    var_int[t,:,j,i] = np.interp(lev_int[::-1], lev[t,::-1,j,i], var[t,::-1,j,i],
                                                 left=np.nan, right=np.nan)[::-1]
        # convert varint into dataarray and add to dataset
        da_varint = xr.DataArray(var_int, name=varname, dims=("time", "lev", "lat", "lon"),
                                 coords={"time": ds.time, "lev": lev_int, "lat": ds.lat, "lon": ds.lon})
        ds_int[varname] = da_varint

    _ds.close()
        
    return ds_int

#-----------------------------------------------

def interpolate2pressure_Z(ds, varnames, lev_int):
    """Interpolate data onto pressure levels using numpy interp. Input data is zonal mean, i.e.
    has no longitude. This is indicated by the _Z in the function name.
    
    lev_int is the target pressure levels in Pa."""

    import xarray as xr
    import numpy as np
    
    # dimensions of input data
    ntim = ds.time.size
    nlat = ds.lat.size

    # output dataset with vertically interpolated 3d fields
    ds_int = xr.Dataset()
    ds_int.attrs = ds.attrs
 
    # compute datset once and load to local dataset
    # this seems to increase performance, but I assume only matters when we use chunked arrays
    _ds = ds.compute()

    # loop over requested variables
    for varname in varnames:
        var  = _ds[varname].values
        lev  = _ds["pres"].values
        var_int = np.zeros((ntim, lev_int.size, nlat)) + np.nan
        for t in range(ntim):
            for j in range(nlat):
                var_int[t,:,j] = np.interp(lev_int[::-1], lev[t,::-1,j], var[t,::-1,j],
                                                 left=np.nan, right=np.nan)[::-1]
        # convert varint into dataarray and add to dataset
        da_varint = xr.DataArray(var_int, name=varname, dims=("time", "lev", "lat"),
                                 coords={"time": ds.time, "lev": lev_int, "lat": ds.lat})
        ds_int[varname] = da_varint
    _ds.close()
        
    return ds_int

#-----------------------------------------------

def interpolate2height(ds, varnames, hgt_int):
    """Interpolate data onto height levels using numpy interp.
    
    hgt_int is the target height levels in m.
    
    Used for HadGEM3 and UKESM models."""

    import xarray as xr
    import numpy as np
    
    # dimensions of input data
    ntim = ds.time.size
    nlat = ds.lat.size
    nlon = ds.lon.size

    # output dataset with vertically interpolated 3d fields
    ds_int = xr.Dataset()
    ds_int.attrs = ds.attrs
 
    # compute datset once and load to local dataset
    # this seems to increase performance, but I assume only matters when we use chunked arrays
    _ds = ds.compute()

    # loop over requested variables
    for varname in varnames:
        var  = _ds[varname].values
        var_int = np.zeros((ntim, hgt_int.size, nlat, nlon)) + np.nan
        # if _ds["b"] and _ds["orog"] are time dependent:
        try:
            hgt  = (_ds["lev"] + _ds["b"] * _ds["orog"]).transpose("time", "lev", "lat", "lon")
            for t in range(ntim):
                for j in range(nlat):
                    for i in range(nlon):
                        var_int[t,:,j,i] = np.interp(hgt_int, hgt[t,:,j,i], var[t,:,j,i],
                                                     left=np.nan, right=np.nan)
        except:
            pass
        # if _ds["b"] and _ds["orog"] are not time dependent:
        try:
            hgt  = (_ds["lev"] + _ds["b"] * _ds["orog"]).transpose("lev", "lat", "lon")
            for t in range(ntim):
                for j in range(nlat):
                    for i in range(nlon):
                        var_int[t,:,j,i] = np.interp(hgt_int, hgt[:,j,i], var[t,:,j,i],
                                                     left=np.nan, right=np.nan)
        except:
            pass
        # convert varint into dataarray and add to dataset
        da_varint = xr.DataArray(var_int, name=varname, dims=("time", "hgt", "lat", "lon"),
                                 coords={"time": ds.time, "hgt": hgt_int, "lat": ds.lat, "lon": ds.lon})
        ds_int[varname] = da_varint

    _ds.close()
        
    return ds_int

#-----------------------------------------------

def compute_heatingrates_modellevels(ds):
    """Derive radiative heating rates directly on model levels for a 
    radiative flux. The function assumes that the radiative flux is called "radflx".
    
    The function assumes that the input dataset ds contains the pressure
    of the radiative fluxes as the variable pres in units of Pa. 
    
    It is also important that the flux divergence is computed as a difference
    between adjacent layers, and not as a higher-order gradient. This is achieved
    by numpy.diff."""

    import xarray as xr
    import numpy as np
    
    # dimensions of input data
    ntim = ds.time.size
    nlev = ds["pres"][0,:,0,0].values.size
    nlat = ds.lat.size
    nlon = ds.lon.size

    _ds = ds.compute()

    # radiative fluxes
    _radflx = _ds["radflx"].values
    
    # pressure levels of the radiative fluxes
    _pres = _ds["pres"].values
    
    # compute heating rates
    _hr   = -86400 * 9.81/1005 * np.diff(_radflx, axis=1)/np.diff(_pres,axis=1)
    
    # pressure levels of heating rates is in the middle of those of the radiative fluxes
    _pres_hr = 0.5*(_pres[:,1:,:,:]+_pres[:,0:nlev-1,:,:])
    
    # output dataset with heating rates
    ds_hr       = xr.Dataset()
    ds_hr.attrs = ds.attrs
        
    ds_hr["dTdt"] = xr.DataArray(_hr, name="dTdt", dims=("time", "lev", "lat", "lon"), 
                                 coords={"time": _ds.time, "lev": np.arange(0,nlev-1), "lat": _ds.lat, "lon": _ds.lon}) 
    ds_hr["pres"] = xr.DataArray(_pres_hr, name="pres_hr", dims=("time", "lev", "lat", "lon"), 
                                 coords={"time": _ds.time, "lev": np.arange(0,nlev-1), "lat": _ds.lat, "lon": _ds.lon}) 
    
    return ds_hr

#-----------------------------------------------

def list_models(var="*",tab="*",exp="*",ripf="*"):
    """ Returns all available models. Sister function to "list_files".
    
    Only criteria that are passed as arguments are used for filtering. 
    Other criteria are replaced by *."""
    import os
    import fnmatch
    import xarray as xr
    path = "/scratch/das/avoigt/cmip6-acre-data/CMIP6/"
    modlist = list()
    for dirpath, dirs, files in os.walk(path): 
        for filename in fnmatch.filter(files,var+"_"+tab+"_*_"+exp+"_"+ripf+"_*.nc"):
            fname = os.path.join(dirpath,filename)
             # only add model name to list if file is intact
            try:
                xr.open_dataset(fname)
                modlist.append(fname.rsplit("/")[-1].rsplit("_")[2])
            except:
                pass  
    # remove duplicates
    modlist=list(dict.fromkeys(modlist))
    return sorted(sorted(modlist), key=len)

#-----------------------------------------------

def list_files(mod="*", var="*", tab="*", exp="*", ripf="*"):
    """ Returns a list of all intact files.
    
    The list can be constrained by specifying a model mod, variable var, table tab, experiment exp, and member ripf. """
    import os
    import fnmatch
    import xarray as xr
    path = "/scratch/das/avoigt/cmip6-acre-data/CMIP6/"
    f_list = list()
    for dirpath, dirs, files in os.walk(path): 
        for filename in fnmatch.filter(files,var+"_"+tab+"_"+mod+"_"+exp+"_"+ripf+"_*.nc"):
            fname = os.path.join(dirpath,filename)
            # only add file to list if file is intact
            try:
                xr.open_dataset(fname)
                f_list.append(fname)
            except:
                pass              
    return sorted(sorted(f_list), key=len)

#-----------------------------------------------

def filelist_allsky_radfluxes(exp, tab, mod="*"):
    rsd = list_files(var="rsd", tab=tab, exp=exp, mod=mod)
    rsu = list_files(var="rsu", tab=tab, exp=exp, mod=mod)
    rld = list_files(var="rld", tab=tab, exp=exp, mod=mod)
    rlu = list_files(var="rlu", tab=tab, exp=exp, mod=mod)
    # find all files that hold all all-sky radiative fluxes
    rsd = [x.replace("rsd", "VAR") for x in rsd]
    rsu = [x.replace("rsu", "VAR") for x in rsu]
    rld = [x.replace("rld", "VAR") for x in rld]
    rlu = [x.replace("rlu", "VAR") for x in rlu]
    # find all files that have all all-sky radiative fluxes
    joint = list(set(rsd) & set(rsu) & set(rld) & set(rlu))
    # replace VAR again with actual variable name
    rsd = [x.replace("VAR", "rsd") for x in joint]
    rsu = [x.replace("VAR", "rsu") for x in joint]
    rld = [x.replace("VAR", "rld") for x in joint]
    rlu = [x.replace("VAR", "rlu") for x in joint]
    return rsd, rsu, rld, rlu

#-----------------------------------------------

def filelist_clrsky_radfluxes(exp, tab, mod="*"):
    rsdcs = list_files(var="rsdcs", tab=tab, exp=exp, mod=mod)
    rsucs = list_files(var="rsucs", tab=tab, exp=exp, mod=mod)
    rldcs = list_files(var="rldcs", tab=tab, exp=exp, mod=mod)
    rlucs = list_files(var="rlucs", tab=tab, exp=exp, mod=mod)
    # find all files that hold all clear-sky radiative fluxes
    rsdcs = [x.replace("rsdcs", "VAR") for x in rsdcs]
    rsucs = [x.replace("rsucs", "VAR") for x in rsucs]
    rldcs = [x.replace("rldcs", "VAR") for x in rldcs]
    rlucs = [x.replace("rlucs", "VAR") for x in rlucs]
    # find all files that have all clear-sky radiative fluxes
    joint = list(set(rsdcs) & set(rsucs) & set(rldcs) & set(rlucs))
    # replace VAR again with actual variable name
    rsdcs = [x.replace("VAR", "rsdcs") for x in joint]
    rsucs = [x.replace("VAR", "rsucs") for x in joint]
    rldcs = [x.replace("VAR", "rldcs") for x in joint]
    rlucs = [x.replace("VAR", "rlucs") for x in joint]
    return rsdcs, rsucs, rldcs, rlucs

#-----------------------------------------------

def filelist_allsky_tntr(exp, tab, mod="*"):
    tntrs = list_files(var="tntrs", tab=tab, exp=exp, mod=mod)
    tntrl = list_files(var="tntrl", tab=tab, exp=exp, mod=mod)
    # find all files that hold all all-sky radiative fluxes
    tntrs = [x.replace("tntrs", "VAR") for x in tntrs]
    tntrl = [x.replace("tntrl", "VAR") for x in tntrl]
    # find all files that have all all-sky radiative fluxes
    joint = list(set(tntrs) & set(tntrl))
    # replace VAR again with actual variable name
    tntrs = [x.replace("VAR", "tntrs") for x in joint]
    tntrl = [x.replace("VAR", "tntrl") for x in joint]
    return tntrs, tntrl

#-----------------------------------------------

def filelist_allsky_tntronly(exp, tab, mod="*"):
    tntr = list_files(var="tntr", tab=tab, exp=exp, mod=mod)
    return tntr

#-----------------------------------------------

def filelist_clrsky_tntr(exp, tab, mod="*"):
    tntrscs = list_files(var="tntrscs", tab=tab, exp=exp, mod=mod)
    tntrlcs = list_files(var="tntrlcs", tab=tab, exp=exp, mod=mod)
    # find all files that hold all clear-sky radiative fluxes
    tntrscs = [x.replace("tntrscs", "VAR") for x in tntrscs]
    tntrlcs = [x.replace("tntrlcs", "VAR") for x in tntrlcs]
    # find all files that have all clear-sky radiative fluxes
    joint = list(set(tntrscs) & set(tntrlcs))
    # replace VAR again with actual variable name
    tntrscs = [x.replace("VAR", "tntrscs") for x in joint]
    tntrlcs = [x.replace("VAR", "tntrlcs") for x in joint]
    return tntrscs, tntrlcs

#-----------------------------------------------

def filelist_ta(exp, tab, mod="*"):
    tntr = list_files(var="ta", tab=tab, exp=exp, mod=mod)
    return tntr

#-----------------------------------------------

def load_data(flist):
    import os
    import fnmatch
    import xarray as xr
    path = "/scratch/das/avoigt/cmip6-acre-data/CMIP6/"
    
    # generate list of models from flist
    modlist = list()
    for f in flist:
        modlist.append(f.rsplit("/")[-1].rsplit("_")[2])
    # remove duplicates
    modlist=list(dict.fromkeys(modlist))
      
    ds_list = list()
    for mod in modlist:
        # get all files for model mod
        flist_mod = list()
        # to distinguish model names such as CESM2 and CESM2-FV2,
        # we need to require that "_"+mod+"_" is in file name, not just mod
        [flist_mod.append(x) for x in flist if "_"+mod+"_" in x];
    
        # screen for ripf number
        ripf_list = list()
        for file in flist_mod:
            ripf = file.rsplit("/")[-1].rsplit("_")[4]
            ripf_list.append(ripf)
        # remove duplicates and sort so that r1i1p1f1 is first when available
        ripf_list=list(dict.fromkeys(ripf_list))
        ripf_list=sorted(sorted(ripf_list), key=len)
           
        # load only files for the first ripf member
        ripf = ripf_list[0]
        flist_mod_ripf = list()
        [flist_mod_ripf.append(x) for x in flist_mod if ripf in x];
        
        # manual correction of date, e.g., for IPSL models
        def correct_data(ds):
            # need to rename level dimensions for IPSL models
            try:
                ds = ds.rename({'klev': 'lev'})
            except:
                try:
                    ds = ds.rename({'klevp1': 'lev'})
                except:
                    pass
            # need to correct sign of rldcs in IPSL-CM6A-LR
            if ds.attrs["source_id"] == "IPSL-CM6A-LR":
                try:
                    ds["rldcs"] = - 1*ds["rldcs"]
                except:
                    pass
            return ds
        
        try:
            ds = xr.open_mfdataset(flist_mod_ripf)
            ds = correct_data(ds)
            ds_list.append(ds)
        except:
            print("Warning: cannot load data for model", mod, " and ripf", ripf, ";", flist_mod_ripf)
         
    return ds_list

#-----------------------------------------------

def add_radflx(ds_list, var_list, sign_list):
    """ Adds radiative fluxes from the input datasets given by ds_list, with
    the variable names given by var_list. The input list sign_list defines
    whether a flux needs to be added (for downward fluxes) or subtracted
    (for upward fluxes).
    
    Output is the radiative flux "radflx". """
    import xarray as xr
    radflx = ds_list[0][var_list[0]]
    for i in range(1, len(ds_list)):
        radflx += sign_list[i]*ds_list[i][var_list[i]]
    radflx.attrs["standard_name"] = "generic radiative flux"
    radflx.attrs["long_name"] = "generic radiative flux" 
    radflx.attrs["comment"] = "generic radiative flux" 
    radflx.attrs["original_name"] = "generic radiative flux"
    # make sure that sfc pressure etc is part of rad flux dataset
    ds_flx = ds_list[0].copy(deep=True)
    ds_flx["radflx"] = xr.DataArray(radflx, name="radflx", dims=("time", "lev", "lat", "lon"), 
                                    coords={"time": ds_list[0].time, "lev": ds_list[0].lev, 
                                            "lat": ds_list[0].lat, "lon": ds_list[0].lon})
    # only keep radflx variable and sfc pressure information 
    ds_flx = ds_flx.drop_vars(var_list[0])
    return ds_flx

#-----------------------------------------------

def add_heatingrate(ds_list, var_list):
    """ Adds heating rates from the input datasets given by ds_list, with
    the variable names given by var_list.
    
    Output is the heating rate "dTdt" in units of K/day. """
    import xarray as xr
    # convert from units of  K s-1 to K day-1
    dTdt = 86400*ds_list[0][var_list[0]]
    for i in range(1, len(ds_list)):
        dTdt += 86400*ds_list[i][var_list[i]]
    dTdt.attrs["standard_name"] = "generic heating rate"
    dTdt.attrs["long_name"] = "generic heating rate" 
    dTdt.attrs["comment"] = "generic heating rate" 
    dTdt.attrs["original_name"] = "generic heating rate"
    dTdt.attrs["units"] = "K day-1"
    # make sure that sfc pressure etc is part of heating rate dataset
    ds_dTdt = ds_list[0].copy(deep=True)
    ds_dTdt["dTdt"] = xr.DataArray(dTdt, name="dTdt", dims=("time", "lev", "lat", "lon"), 
                                    coords={"time": ds_list[0].time, "lev": ds_list[0].lev, 
                                            "lat": ds_list[0].lat, "lon": ds_list[0].lon})
    # only keep tntr variable and sfc pressure information 
    ds_dTdt = ds_dTdt.drop_vars(var_list[0])
    return ds_dTdt

#-----------------------------------------------

def add_heatingrate_Z(ds_list, var_list):
    """ Adds heating rates from the input datasets given by ds_list, with
    the variable names given by var_list. Input data is zonal mean, i.e.
    has no longitude. This is indicated by the _Z in the function name.
    
    Output is the heating rate "dTdt" in units of K/day. """
    import xarray as xr
    # convert from units of  K s-1 to K day-1
    dTdt = 86400*ds_list[0][var_list[0]]
    for i in range(1, len(ds_list)):
        dTdt += 86400*ds_list[i][var_list[i]]
    dTdt.attrs["standard_name"] = "generic heating rate"
    dTdt.attrs["long_name"] = "generic heating rate" 
    dTdt.attrs["comment"] = "generic heating rate" 
    dTdt.attrs["original_name"] = "generic heating rate"
    dTdt.attrs["units"] = "K day-1"
    # make sure that sfc pressure etc is part of heating rate dataset
    ds_dTdt = ds_list[0].copy(deep=True)
    ds_dTdt["dTdt"] = xr.DataArray(dTdt, name="dTdt", dims=("time", "lev", "lat"), 
                                    coords={"time": ds_list[0].time, "lev": ds_list[0].lev, 
                                            "lat": ds_list[0].lat})
    # only keep tntr variable and sfc pressure information 
    ds_dTdt = ds_dTdt.drop_vars(var_list[0])
    return ds_dTdt