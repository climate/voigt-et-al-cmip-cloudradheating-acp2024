#!/usr/bin/env python
# coding: utf-8

# # Manual treatment of GFDL-CM4 tntr data for amip-p4K and amip-future4K simulations
# on srvx1 of IMG UNIVIE call as
# /home/swd/manual/nwp/2023.1/bin/python3.10 compute_tntr_GFDL-CM4_amipp4K_amipfuture4K.py

path="/scratch/das/avoigt/cmip6-acre-data/CMIP6/"

import sys
sys.path.append("/users/staff/avoigt/cmip-acre/analysis")
import cmip6radheating
import xarray as xr


# ## 0. Define functions to compute radiative heating rate from tntr, taken from batchcompute_dtdt-from-tntr.py

# function that computes radiative heating rates
# below we loop over the function using multiprocessing
def compute_dtdt_from_tntr(_ds_list, _var_list, _varout):
    """ Computes heating rates for a list of one or several data sets
    that each contain a radiative heating rate. The net radiative 
    heating rate is calculated as the sum of the input radiative heating rates.
    
    _ds_list: list of radiative heating rate datasets
    _var_list: list of the variable names of the radiative heating rates in _ds_list
    """
    import shutil    
    _model = _ds_list[0].attrs["source_id"]
    # compute net radiative heating rate from the sum of input radiative heating rates
    _dtdt = cmip6radheating.add_heatingrate(_ds_list, _var_list)
    _dtdt = cmip6radheating.addpressure2dataset(_dtdt, model=_model, levtype="full")
    _levint = cmip6radheating.define_targetlevels()
    _dtdt_int = cmip6radheating.interpolate2pressure(_dtdt, ["dTdt"], _levint).rename({'dTdt': _varout})
    # write interpolated heating rate to zarr store
    _path = "/scratch/das/avoigt/cmip6-acre-data/postprocessed/"
    _mod = _dtdt_int.attrs["source_id"]
    _exp = _dtdt_int.attrs["experiment_id"]
    _tab = _dtdt_int.attrs["table_id"]
    _out = _path+_varout+"_"+_tab+"_"+_mod+"_"+_exp+".zarr"
    try:
        shutil.rmtree(_out, ignore_errors=False, onerror=None)
    except:
        pass
    _dtdt_int.to_zarr(_out)
    print(_model, "finished")
    
# function that computes radiative heating rates for EMonZ table data, , taken from batchcompute_dtdt-from-tntr_EmonZ.py
def compute_dtdt_from_tntr_EmonZ(_ds_list, _var_list, _varout):
    """ Computes heating rates for a list of one or several data sets
    that each contain a radiative heating rate. The net radiative 
    heating rate is calculated as the sum of the input radiative heating rates.
    
    _ds_list: list of radiative heating rate datasets
    _var_list: list of the variable names of the radiative heating rates in _ds_list
    """
    import shutil    
    _model = _ds_list[0].attrs["source_id"]
    _levint = cmip6radheating.define_targetlevels()
    try: 
        # add pressure to datasets, note that EmonZ data is already on pressure levels
        _ds_list_withlev = list()
        for i in range(0, len(_ds_list)):
            _ds = _ds_list[i]
            _ds["pres"] = (_ds.plev + 0.0*_ds[_var_list[i]]).transpose("time", "plev", "lat").rename({"plev": "lev"})
            _ds_list_withlev.append(_ds)
        # compute net radiative heating rate from the sum of input radiative heating rates
        _dtdt = cmip6radheating.add_heatingrate_Z(_ds_list_withlev, _var_list)
        # interpolate to new pressure levels
        _dtdt_int = cmip6radheating.interpolate2pressure_Z(_dtdt, ["dTdt"], _levint).rename({'dTdt': _varout})
        # write interpolated heating rate to zarr store
        _path = "/scratch/das/avoigt/cmip6-acre-data/postprocessed/"
        _mod = _dtdt_int.attrs["source_id"]
        _exp = _dtdt_int.attrs["experiment_id"]
        _tab = _dtdt_int.attrs["table_id"]
        _out = _path+_varout+"_"+_tab+"_"+_mod+"_"+_exp+".zarr"
        try:
            shutil.rmtree(_out, ignore_errors=False, onerror=None)
        except:
            pass
        _dtdt_int.to_zarr(_out)
        print(_model, "finished")
    except:
        print(_model, "cannot be calculated")
    

## We are only workin on GFDL-CM4 model 
mod="GFDL-CM4"  
    
# ## 1. amip-p4K
#   * we use tntr from CFmon
#   * we use tntrs and tntrl from AERmon
#   * we use tntrscs and tntrlcs from Emon

exp="amip-p4K"

tab="CFmon"
flist_tntr = cmip6radheating.filelist_allsky_tntronly(exp=exp, tab=tab, mod=mod)
ds_tntr = cmip6radheating.load_data(flist_tntr)[0]

tab="AERmon"
flist_tntrs, flist_tntrl = cmip6radheating.filelist_allsky_tntr(exp=exp, tab=tab, mod=mod)
ds_tntrs = cmip6radheating.load_data(flist_tntrs)[0]
ds_tntrl = cmip6radheating.load_data(flist_tntrl)[0]

tab="Emon"
flist_tntrscs, flist_tntrlcs = cmip6radheating.filelist_clrsky_tntr(exp=exp, tab=tab, mod=mod)
ds_tntrscs = cmip6radheating.load_data(flist_tntrscs)[0]
ds_tntrlcs = cmip6radheating.load_data(flist_tntrlcs)[0]

# Adds ps to tntr datasets.
ds_ps = xr.open_dataset(path+"/amip-p4K/ps/Amon/ps_Amon_GFDL-CM4_amip-p4K_r1i1p1f1_gr1_197901-201412.nc")
ds_tntr    = xr.merge([ds_tntr   , ds_ps])
ds_tntrs   = xr.merge([ds_tntrs  , ds_ps])
ds_tntrl   = xr.merge([ds_tntrl  , ds_ps])
ds_tntrscs = xr.merge([ds_tntrscs, ds_ps])
ds_tntrlcs = xr.merge([ds_tntrlcs, ds_ps])

# compute radiative heating rates
compute_dtdt_from_tntr([ds_tntr], ["tntr"], "dTdt-as-from-tntr")

compute_dtdt_from_tntr([ds_tntrs, ds_tntrl], ["tntrs", "tntrl"], "dTdt-as-from-tntr")
compute_dtdt_from_tntr([ds_tntrs], ["tntrs"], "dTdts-as-from-tntr")
compute_dtdt_from_tntr([ds_tntrl], ["tntrl"], "dTdtl-as-from-tntr")

compute_dtdt_from_tntr([ds_tntrscs, ds_tntrlcs], ["tntrscs", "tntrlcs"], "dTdt-cs-from-tntr")
compute_dtdt_from_tntr([ds_tntrscs], ["tntrscs"], "dTdts-cs-from-tntr")
compute_dtdt_from_tntr([ds_tntrlcs], ["tntrlcs"], "dTdtl-cs-from-tntr")

# clean up
del flist_tntr, flist_tntrs, flist_tntrl, flist_tntrscs, flist_tntrlcs
del ds_tntr, ds_tntrs, ds_tntrl, ds_tntrscs, ds_tntrlcs


# ## 2. amip-future4K
#   * we use tntr from CFmon
#   * we use tntrs, tntrscs, tntrl, tntrlcs from EmonZ, which are already on pressure levels

exp="amip-future4K"

tab="CFmon"
flist_tntr = cmip6radheating.filelist_allsky_tntronly(exp=exp, tab=tab, mod=mod)
ds_tntr = cmip6radheating.load_data(flist_tntr)[0]

tab="EmonZ"
flist_tntrs, flist_tntrl = cmip6radheating.filelist_allsky_tntr(exp=exp, tab=tab, mod=mod)
ds_tntrs = cmip6radheating.load_data(flist_tntrs)[0]
ds_tntrl = cmip6radheating.load_data(flist_tntrl)[0]
flist_tntrscs, flist_tntrlcs = cmip6radheating.filelist_clrsky_tntr(exp=exp, tab=tab, mod=mod)
ds_tntrscs = cmip6radheating.load_data(flist_tntrscs)[0]
ds_tntrlcs = cmip6radheating.load_data(flist_tntrlcs)[0]

# Adds ps to tntr datasets, only needed for tntr from CFmon table
ds_ps = xr.open_dataset(path+"/amip-future4K/ps/Amon/ps_Amon_GFDL-CM4_amip-future4K_r1i1p1f1_gr1_197901-201412.nc")
ds_tntr    = xr.merge([ds_tntr   , ds_ps])

# compute radiative heating rates
compute_dtdt_from_tntr([ds_tntr], ["tntr"], "dTdt-as-from-tntr")

compute_dtdt_from_tntr_EmonZ([ds_tntrs, ds_tntrl], ["tntrs", "tntrl"], "dTdt-as-from-tntr")
compute_dtdt_from_tntr_EmonZ([ds_tntrs], ["tntrs"], "dTdts-as-from-tntr")
compute_dtdt_from_tntr_EmonZ([ds_tntrl], ["tntrl"], "dTdtl-as-from-tntr")

compute_dtdt_from_tntr_EmonZ([ds_tntrscs, ds_tntrlcs], ["tntrscs", "tntrlcs"], "dTdt-cs-from-tntr")
compute_dtdt_from_tntr_EmonZ([ds_tntrscs], ["tntrscs"], "dTdts-cs-from-tntr")
compute_dtdt_from_tntr_EmonZ([ds_tntrlcs], ["tntrlcs"], "dTdtl-cs-from-tntr")
