#!/usr/bin/env python
# coding: utf-8
#
# Make climatological means over zarr stores of postprocessed data 
#
# on srvx1 of img univie, call as follows:
# /home/swd/manual/nwp/2023.1/bin/python3.10 make_mean_data.py

import xarray as xr
import os
import subprocess

def removefirst12months(ds):
    """Removes the first 12 months of a dataset."""
    ntim = ds.time.size
    ds = ds.isel(time=slice(12, ntim))
    return ds

# directory of zarr stores for which mean is computed
path="/scratch/das/avoigt/cmip6-acre-data/postprocessed/"

# location of computes means
path_mean=path+"/mean/"

# create directory for means if it does not already exist
subprocess.run(["mkdir", "-p", path_mean])

# switches to only process part of the data
lzarr=False
lnetcdf=False
lzarr_cl=True

# loop over all zarr stores
if lzarr:
    for zstore in os.listdir(path):
        if ".zarr" in str(zstore):
            try:
                fname=str(zstore).split(".")[0]
                ds = xr.open_dataset(path+"/"+zstore, chunks="auto", engine="zarr")
                ds = removefirst12months(ds)
                ds_clim = ds.groupby("time.month").mean("time")
                # set time axis
                ds_clim["time"] = ds.time[0:12]        
                # include first and time step that enters the climatology as dataset attributes
                ds_clim.attrs["firstimestep"] = str(ds.time[0].values)
                ds_clim.attrs["lasttimestep"] = str(ds.time[-1].values)
                # output netcdf file name
                ncout = path_mean+"/"+fname+".ymonmean.nc"
                # remove ncout if it already exists, needed as otherwise nc write operation might fail in unpredictable manner
                subprocess.run(["rm", "-f", ncout])
                ds_clim.to_netcdf(ncout, mode="w")
            except:
                print("Mean operation failed for:", zstore)
                pass
        
# loop over all netcdf files
if lnetcdf:
    for ncfile in os.listdir(path):
        if ".nc" in str(ncfile):
            try:
                fname=str(ncfile).split(".")[0]
                ds = xr.open_dataset(path+"/"+ncfile)
                ds = removefirst12months(ds)
                ds_clim = ds.groupby("time.month").mean("time")
                # set time axis
                ds_clim["time"] = ds.time[0:12]        
                # include first and time step that enters the climatology as dataset attributes
                ds_clim.attrs["firstimestep"] = str(ds.time[0].values)
                ds_clim.attrs["lasttimestep"] = str(ds.time[-1].values)
                # output netcdf file name
                ncout = path_mean+"/"+fname+".ymonmean.nc"
                # remove ncout if it already exists, needed as otherwise nc write operation might fail in unpredictable manner
                subprocess.run(["rm", "-f", ncout])
                ds_clim.to_netcdf(ncout, mode="w")
            except:
                print("Mean operation failed for:", ncfile)
                pass
            
# loop over all zarr stores with cloud fraction data
if lzarr_cl:
    for zstore in os.listdir(path):
        if "cl_Amon" in str(zstore) and ".zarr" in str(zstore):
            try:
                fname=str(zstore).split(".")[0]
                ds = xr.open_dataset(path+"/"+zstore, chunks="auto", engine="zarr")
                ds = removefirst12months(ds)
                ds_clim = ds.groupby("time.month").mean("time")
                # set time axis
                ds_clim["time"] = ds.time[0:12]        
                # include first and time step that enters the climatology as dataset attributes
                ds_clim.attrs["firstimestep"] = str(ds.time[0].values)
                ds_clim.attrs["lasttimestep"] = str(ds.time[-1].values)
                # output netcdf file name
                ncout = path_mean+"/"+fname+".ymonmean.nc"
                # remove ncout if it already exists, needed as otherwise nc write operation might fail in unpredictable manner
                subprocess.run(["rm", "-f", ncout])
                ds_clim.to_netcdf(ncout, mode="w")
            except:
                print("Mean operation failed for:", zstore)
                pass
