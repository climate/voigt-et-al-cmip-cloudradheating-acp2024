#!/usr/bin/env python
# coding: utf-8
#
# on srvx1 call as
# /home/swd/manual/nwp/2023.1/bin/python3.10 batchcompute_ta_Amon.py amip 

import sys
args = sys.argv
# args[0]: name of the script
# args[1]: experiment, first argument passed in
# args[2]: table, second argument passed in
# etc.
exp=args[1]
tab="Amon"

import warnings
warnings.simplefilter("ignore") 

import cmip6radheating
from multiprocessing import Process

flist = cmip6radheating.filelist_ta(exp=exp, tab=tab)
ds = cmip6radheating.load_data(flist)

# function that interpolates ta to pressure target levels, note that ta from Amon table
# is already on pressure levels
# below we loop over the function using multiprocessing
def interpolate_ta_Amon(_ds):
    """ Interpolates temperature from Amon pressure levels to target pressure levels.
    
    _ds: input xarray dataset with ta
    """
    import shutil    
    _model = _ds.attrs["source_id"]
    _levint = cmip6radheating.define_targetlevels()
    try: 
        # add pressure to datasets, note that EmonZ data is already on pressure levels
        _ds["pres"] = (_ds.plev + 0.0*_ds["ta"]).transpose("time", "plev", "lat", "lon").rename({"plev": "lev"})
        # interpolate to new pressure levels
        _ds_int = cmip6radheating.interpolate2pressure(_ds, ["ta"], _levint)
        # write interpolated heating rate to zarr store
        _path = "/scratch/das/avoigt/cmip6-acre-data/postprocessed/"
        _mod = _ds_int.attrs["source_id"]
        _exp = _ds_int.attrs["experiment_id"]
        _tab = _ds_int.attrs["table_id"]
        _out = _path+"ta_"+_tab+"_"+_mod+"_"+_exp+".zarr"
        try:
            shutil.rmtree(_out, ignore_errors=False, onerror=None)
        except:
            pass
        _ds_int.to_zarr(_out)
        print(_model, "finished")
    except:
        print(_model, "cannot be calculated")

        
processes = [Process(target=interpolate_ta_Amon, 
                     args=(ds[i],)) for i in range(0, len(ds))]
for process in processes: process.start() # start all processes
for process in processes: process.join()  # wait for all processes to complete