#!/usr/bin/env python
# coding: utf-8
#
# on IMG SRVX1, call the python script as:
# /home/swd/manual/nwp/2023.1/bin/python3.10 compute_dtdt-from-tntr_HadGEM3-GC31-LL_UKESM1-0-LL.py

# # Postprocess heating rates for UK and HadGEM models, which are on height levels
# Models of interest: "HadGEM3-GC31-LL", "UKESM1-0-LL".

import sys
sys.path.append("/users/staff/avoigt/cmip-acre/analysis")
import cmip6radheating

import numpy as np
import xarray as xr
import zarr


# Defines function that contains all steps to compute radiative heating rate from tntr variables.
def compute_dtdt_from_tntr(_ds_list, _var_list, _varout):
    """ Computes heating rates for a list of one or several data sets
    that each contain a radiative heating rate. The net radiative 
    heating rate is calculated as the sum of the input radiative heating rates.
    
    _ds_list: list of radiative heating rate datasets
    _var_list: list of the variable names of the radiative heating rates in _ds_list
    """
    import shutil    
    _model = _ds_list[0].attrs["source_id"]
    # compute net radiative heating rate from the sum of input radiative heating rates
    _dtdt = cmip6radheating.add_heatingrate(_ds_list, _var_list)
    # height levels for interpolation
    _hgtint = np.arange(0,20e3,2e2)
    _dtdt_int = cmip6radheating.interpolate2height(_dtdt, ["dTdt"], _hgtint).rename({'dTdt': _varout})
    # write interpolated heating rate to zarr store
    _path = "/scratch/das/avoigt/cmip6-acre-data/postprocessed/"
    _mod = _dtdt_int.attrs["source_id"]
    _exp = _dtdt_int.attrs["experiment_id"]
    _tab = _dtdt_int.attrs["table_id"]
    _out = _path+_varout+"_"+_tab+"_"+_mod+"_"+_exp+".zarr"
    try:
        shutil.rmtree(_out, ignore_errors=False, onerror=None)
    except:
        pass
    _dtdt_int.to_zarr(_out)


# Loops over experiments, tables and the two models.
for exp in ["amip", "amip-p4K", "amip-future4K", "aqua-control", "aqua-p4K"]:
    for tab in ["AERmon", "Emon"]:
        for mod in ["HadGEM3-GC31-LL", "UKESM1-0-LL"]:
            # all-sky radiative heating
            print("Working on all-sky radiative heating", exp, tab, mod)
            flist_tntrs, flist_tntrl = cmip6radheating.filelist_allsky_tntr(exp=exp, 
                                                                            tab=tab, mod=mod)
            if len(flist_tntrs)>0 and len(flist_tntrl)>0:
                ds_tntrs = (cmip6radheating.load_data(flist_tntrs)[0])
                ds_tntrl = (cmip6radheating.load_data(flist_tntrl)[0])
                compute_dtdt_from_tntr([ds_tntrs, ds_tntrl], ["tntrs", "tntrl"], 
                                        "dTdt-as-from-tntr")
                compute_dtdt_from_tntr([ds_tntrs], ["tntrs"], "dTdts-as-from-tntr")
                compute_dtdt_from_tntr([ds_tntrl], ["tntrl"], "dTdtl-as-from-tntr")
                print("Finished all-sky computation for", exp, tab, mod)
            else:
                print("No all-sky data available for", exp, tab, mod, "; hence no computation done")
            # clear-sky radiative heating
            print("Working on clr-sky radiative heating", exp, tab, mod)
            flist_tntrscs, flist_tntrlcs = cmip6radheating.filelist_clrsky_tntr(exp=exp, 
                                                                                tab=tab, mod=mod)
            if len(flist_tntrscs)>0 and len(flist_tntrlcs)>0:
                ds_tntrscs = (cmip6radheating.load_data(flist_tntrscs)[0])
                ds_tntrlcs = (cmip6radheating.load_data(flist_tntrlcs)[0])
                compute_dtdt_from_tntr([ds_tntrscs, ds_tntrlcs], ["tntrscs", "tntrlcs"], 
                                        "dTdt-cs-from-tntr")
                compute_dtdt_from_tntr([ds_tntrscs], ["tntrscs"], "dTdts-cs-from-tntr")
                compute_dtdt_from_tntr([ds_tntrlcs], ["tntrlcs"], "dTdtl-cs-from-tntr")
                print("Finished clr-sky computation for", exp, tab, mod)
            else:
                print("No clr-sky data available for", exp, tab, mod, "; hence no computation done")