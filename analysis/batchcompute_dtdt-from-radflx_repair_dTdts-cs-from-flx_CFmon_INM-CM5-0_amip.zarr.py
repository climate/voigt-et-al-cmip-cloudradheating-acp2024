#!/usr/bin/env python
# coding: utf-8
#
# on srvx1 call as
# /home/swd/manual/nwp/2023.1/bin/python3.10 batchcompute_dtdt-from-radflx.py amip CFmon

import sys
#args = sys.argv
## args[0]: name of the script
## args[1]: experiment, first argument passed in
## args[2]: table, second argument passed in
## etc.
#exp=args[1]
#tab=args[2]

exp="amip"
tab="CFmon"
mod="INM-CM5-0" 

import cmip6radheating
from multiprocessing import Process

flist_rsdcs, flist_rsucs, _, _ = cmip6radheating.filelist_clrsky_radfluxes(exp=exp, tab=tab, mod=mod)
ds_rsdcs = cmip6radheating.load_data(flist_rsdcs)
ds_rsucs = cmip6radheating.load_data(flist_rsucs)

# below we loop over the function using multiprocessing
def compute_dtdt_from_flx(_ds_list, _var_list, _sign_list, _varout):
    """ Computes heating rates for a list of one or several data sets
    that each contain a radiative flux. The net radiative 
    flux is calculated as the sum or difference of the input radiative fluxes.
    
    _ds_list: list of radiative flux datasets
    _var_list: list of the variable names of the radiative fluxes in _ds_list
    _sign_list: list of +-1 that defines whether a flux needs to be added or subtracted
    """
    import shutil    
    _model = _ds_list[0].attrs["source_id"]
    # compute net radiative flux from the sum of input radiative fluxes
    _ds_flx = cmip6radheating.add_radflx(_ds_list, _var_list, _sign_list)
    try:
        _ds_flx = cmip6radheating.addpressure2dataset(_ds_flx, model=_model, levtype="half")
        _dtdt   = cmip6radheating.compute_heatingrates_modellevels(_ds_flx)
        _levint = cmip6radheating.define_targetlevels()
        _dtdt_int = cmip6radheating.interpolate2pressure(_dtdt, ["dTdt"], _levint).rename({'dTdt': _varout})
        # write interpolated heating rate to zarr store
        _path = "/scratch/das/avoigt/cmip6-acre-data/postprocessed/"
        _mod = _dtdt_int.attrs["source_id"]
        _exp = _dtdt_int.attrs["experiment_id"]
        _tab = _dtdt_int.attrs["table_id"]
        _out = _path+_varout+"_"+_tab+"_"+_mod+"_"+_exp+".zarr"
        try:
            shutil.rmtree(_out, ignore_errors=False, onerror=None)
        except:
            pass
        _dtdt_int.to_zarr(_out)
        print(_model, "finished")
    except:
        print(_model, "cannot be calculated")


# clear-sky radiative heating rate
processes = [Process(target=compute_dtdt_from_flx, 
                     args=([ds_rsdcs[i], ds_rsucs[i]], 
                           ["rsdcs", "rsucs"], [+1, -1], "dTdts-cs-from-flx"),) for i in range(0, len(ds_rsdcs))]
for process in processes: process.start() # start all processes
for process in processes: process.join()  # wait for all processes to complete
