#!/usr/bin/env python
# coding: utf-8
#
# on srvx1 call as
# /home/swd/manual/nwp/2023.1/bin/python3.10 batchcompute_cl.py amip Amon
#
# interpolate cloud fraction from model levels to pressure levels or height levels (for HadGEM and UK models)

import warnings
warnings.simplefilter("ignore") 

import sys
args = sys.argv
# args[0]: name of the script
# args[1]: experiment, first argument passed in
# args[2]: table, second argument passed in
# etc.
exp=args[1]
tab=args[2]

import cmip6radheating
from multiprocessing import Process

# list of models for which we also have cloud-radiative heating rates
# we only calculate cloud cover for these models, but note that cloud
# cover is not available for all of these models
modlist=["BCC-CSM2-MR", "CESM2", "CESM2-FV2", "CESM2-WACCM", "CESM2-WACCM-FV2", "CNRM-CM6-1",
         "CNRM-ESM2-1", "EC-Earth3", "GFDL-AM4", "GFDL-CM4", "GFDL-ESM4", "HadGEM3-GC31-LL", 
         "HadGEM3-GC31-MM", "INM-CM4-8", "INM-CM5-0", "IPSL-CM6A-LR", "MIROC-ES2L", "MIROC6", 
         "MRI-ESM2-0", "UKESM1-0-LL"]


flist = []
for mod in modlist:
    templist = cmip6radheating.list_files(var="cl", tab=tab, exp=exp, mod=mod)
    [flist.append(temp) for temp in templist]
ds_list = cmip6radheating.load_data(flist)

print("The script will work on the following models:")
for aux in ds_list:
    print(aux.attrs["source_id"])
print("This mean a total number of models of ", len(ds_list)) 
    

def compute_cl(_ds, _exp):
    """ Interpolate cloud cover from model levels to pressure levels.
    
    input: _ds: a single cloud cover dataset 
    """
    import shutil    
    import traceback
    import xarray as xr
    
    _model = _ds.attrs["source_id"]
    try:
        # special treatment for GFDL-AM4 and GFDL-CM4 --> need to add ps
        if _model=="GFDL-AM4":
            if _exp=="amip":
                _ps=xr.open_mfdataset("/scratch/das/avoigt/cmip6-acre-data/CMIP6/amip/ps/Amon/ps_Amon_GFDL-AM4_amip_r1i1p1f1_gr1_198001-201412.nc")
                _ds["ps"]=_ps["ps"]
        if _model=="GFDL-CM4":
            if exp=="amip":
                _ps=xr.open_mfdataset("/scratch/das/avoigt/cmip6-acre-data/CMIP6/amip/ps/Amon/ps_Amon_GFDL-CM4_amip_r1i1p1f1_gr1_*01-*12.nc")
                _ds["ps"]=_ps["ps"]
            if _exp=="amip-p4K":
                _ps=xr.open_dataset("/scratch/das/avoigt/cmip6-acre-data/CMIP6/amip-p4K/ps/Amon/ps_Amon_GFDL-CM4_amip-p4K_r1i1p1f1_gr1_197901-201412.nc")
                _ds["ps"]=_ps["ps"]
        cmip6radheating.addpressure2dataset(_ds, model=_model, levtype="full")
        # need special treatment for IPSL-CM6A-LR --> move from half to full levels
        if _model=="IPSL-CM6A-LR":
            _pres=0.5*(_ds["pres"].isel(lev=slice(0,79)).values + _ds["pres"].isel(lev=slice(1,80)).values)
            _lev=_ds.lev[0:79]
            _ds=_ds.drop(["lev","pres","ap","b","ap_bnds","b_bnds"])
            _ds["pres"] = xr.DataArray(_pres, name="pres", dims=("time", "lev", "lat", "lon"), 
                                       coords={"time": _ds.time, "lev": _lev , "lat": _ds.lat, "lon": _ds.lon})
        _levint = cmip6radheating.define_targetlevels()
        _ds_int = cmip6radheating.interpolate2pressure(_ds, ["cl"], _levint)
        # write interpolated cloud cover to zarr store
        _path = "/scratch/das/avoigt/cmip6-acre-data/postprocessed/"
        _mod = _ds_int.attrs["source_id"]
        _exp = _ds_int.attrs["experiment_id"]
        _tab = _ds_int.attrs["table_id"]
        _out = _path+"/cl_"+_tab+"_"+_mod+"_"+_exp+".zarr"
        try:
            shutil.rmtree(_out, ignore_errors=False, onerror=None)
        except:
            pass
        _ds_int.to_zarr(_out)
        print(_model, "successfully calculated from model levels to pressure levels!")
    except Exception as error: 
        print("---------------------------------------------------------")
        print(_model, "cannot be calculated for model levels to pressure")
        print(error)
        traceback.print_exc()
        print("---------------------------------------------------------")
        
    # special treatment of hadgem and uk models, which use height-based grid
    if _model in ["HadGEM3-GC31-LL", "HadGEM3-GC31-MM", "UKESM1-0-LL"]:
        try:
            _hgtint = cmip6radheating.define_targetlevels_hgt()
            _ds_int = cmip6radheating.interpolate2height(_ds, ["cl"], _hgtint)
            # write interpolated heating rate to zarr store
            _path = "/scratch/das/avoigt/cmip6-acre-data/postprocessed/"
            _mod = _ds_int.attrs["source_id"]
            _exp = _ds_int.attrs["experiment_id"]
            _tab = _ds_int.attrs["table_id"]
            _out = _path+"/cl_"+_tab+"_"+_mod+"_"+_exp+".zarr"
            try:
                shutil.rmtree(_out, ignore_errors=False, onerror=None)
            except:
                pass
            _ds_int.to_zarr(_out)
            print(_model, "successfully calculated from model levels to height levels")
        except Exception as error:
            print("---------------------------------------------------------")
            print(_model, "cannot be calculated for model height levels to height")
            print(error)
            traceback.print_exc()
            print("---------------------------------------------------------")
            
        
processes = [Process(target=compute_cl, 
                     args=(ds_list[i], exp),) for i in range(0, len(ds_list))]
for process in processes: process.start() # start all processes
for process in processes: process.join()  # wait for all processes to complete
