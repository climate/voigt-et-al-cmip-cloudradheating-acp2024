#!/usr/bin/env python
# coding: utf-8

# on srvx1 call as
# /home/swd/manual/nwp/2023.1/bin/python3.10 compute_dtdt-from-flx_HadGEM3-GC31-LL_amip-future4K_CFmon.py

# # Postprocess heating rates for amip-future4K simulation of HadGEM3-GC31-LL model from radiative fluxes
# 
# The HadGEM3-GC31-LL  model is on height levels and we need to use the CMIP6-provided all-sky heating rate tntr to convert CFmon fluxes to heating rates. This is needed since the radiative flux files do not contain air density.

import sys
sys.path.append("/users/staff/avoigt/cmip-acre/analysis")
import cmip6radheating
import numpy as np
import xarray as xr

# ### First load data.

# We only consider the amip-future4K simulation and radiative fluxes from the CFmon table.

exp="amip-future4K"
tab="CFmon"
mod="HadGEM3-GC31-LL"

# Loads radiative fluxes.
flist_rsd, flist_rsu, flist_rld, flist_rlu = cmip6radheating.filelist_allsky_radfluxes(exp=exp, tab=tab, mod=mod)
ds_rsd = cmip6radheating.load_data(flist_rsd)[0]
ds_rsu = cmip6radheating.load_data(flist_rsu)[0]
ds_rld = cmip6radheating.load_data(flist_rld)[0]
ds_rlu = cmip6radheating.load_data(flist_rlu)[0]
flist_rsdcs, flist_rsucs, flist_rldcs, flist_rlucs = cmip6radheating.filelist_clrsky_radfluxes(exp=exp, tab=tab, mod=mod)
ds_rsdcs = cmip6radheating.load_data(flist_rsdcs)[0]
ds_rsucs = cmip6radheating.load_data(flist_rsucs)[0]
ds_rldcs = cmip6radheating.load_data(flist_rldcs)[0]
ds_rlucs = cmip6radheating.load_data(flist_rlucs)[0]

# Loads tntr, which we need for the conversion of flux divergence to heating rate.
flist_tntr = cmip6radheating.filelist_allsky_tntronly(exp=exp, tab=tab, mod=mod)
ds_tntr = (cmip6radheating.load_data(flist_tntr)[0])

# Merges data into a single dataset. Radiative flux data is on levhalf, tntr on lev vertical grid.
ds_flx = ( xr.merge([ds_rsd, ds_rsu, ds_rld, ds_rlu, ds_rsdcs, ds_rsucs, ds_rldcs, ds_rlucs]).
          drop_vars("orog").rename({"b": "bhalf", "lev": "levhalf"}) )
ds = xr.merge([ds_flx, ds_tntr])
del ds_flx, ds_rsd, ds_rsu, ds_rld, ds_rlu, ds_rsdcs, ds_rsucs, ds_rldcs, ds_rlucs, ds_tntr


# ### Introduce functions to convert fluxes to heating rates.

def compute_flux_divergence(flx):
    """Computes vertical divergence of input radiative flux flx on levhalf levels. Output is on lev levels."""
    flxdiv = flx.diff(dim="levhalf", n=1).rename({"levhalf": "lev"})
    flxdiv = xr.where(flxdiv==0.0, np.nan, flxdiv)
    flxdiv = flxdiv.rename("flxdiv").assign_coords(lev=ds.lev)
    return flxdiv

def compute_conv_factor(ds):
    """Computes conversion factor from flux divergence to heating rate from CMIP6 
    provided tntr all-sky heating rate and derived all-sky flux divergence."""
    #all-sky radiative flux
    asflx = (ds["rsd"]-ds["rsu"]+ds["rld"]-ds["rlu"]).rename("radflx")
    # back out conversion factor as data array
    conv = (ds["tntr"] / compute_flux_divergence(asflx)).rename("conv").assign_coords(lev=ds.lev).astype("float64")
    del asflx
    return conv

def compute_dTdt_from_flx(flx, conv):
    """Computes heating rate dTdt in K/day from input fields of radiative flux and conversion factor.
       Output is an xarray  data array."""
    flxdiv = compute_flux_divergence(flx)
    dTdt = 86400*(conv * flxdiv).rename("dTdt")
    # note: replace +-inf with nan
    dTdt = xr.where(dTdt==+np.inf, np.nan, dTdt)
    dTdt = xr.where(dTdt==-np.inf, np.nan, dTdt)
    return dTdt

# Computes conversion factor.
conv = compute_conv_factor(ds)

# ### Now work through the radiative fluxes.
import shutil

def compute_dTdt_save2zarr(radflx, conv, ds, varout):
    """"Wrapper function to i) compute heating rate from input datarray radflx, ii) interpolate to height levels,
        and iii) save to zarr store."""
    # i) compute heating rate
    dTdt = xr.merge([compute_dTdt_from_flx(radflx, conv), ds.b, ds.orog])
    # ii) vertical interpolation
    hgtint = cmip6radheating.define_targetlevels_hgt() # target heights
    dTdt_int = cmip6radheating.interpolate2height(dTdt, ["dTdt"], hgtint).rename({"dTdt": varout})
    # iii) save to zarr store
    path = "/scratch/das/avoigt/cmip6-acre-data/postprocessed/"
    out = path+varout+"_CFmon_"+mod+"_"+exp+".zarr"
    try:
        shutil.rmtree(out, ignore_errors=False, onerror=None)
    except:
        pass
    dTdt_int.to_zarr(out)

# All-sky heating rates.

flx = (ds["rsd"]-ds["rsu"]+ds["rld"]-ds["rlu"]).rename("radflx")
compute_dTdt_save2zarr(flx, conv, ds, varout="dTdt-as-from-flx")
del flx

flx = (ds["rsd"]-ds["rsu"]).rename("radflx")
compute_dTdt_save2zarr(flx, conv, ds, varout="dTdts-as-from-flx")
del flx

flx = (ds["rld"]-ds["rlu"]).rename("radflx")
compute_dTdt_save2zarr(flx, conv, ds, varout="dTdtl-as-from-flx")
del flx

# Clear-sky heating rates.

flx = (ds["rsdcs"]-ds["rsucs"]+ds["rldcs"]-ds["rlucs"]).rename("radflx")
compute_dTdt_save2zarr(flx, conv, ds, varout="dTdt-cs-from-flx")
del flx

flx = (ds["rsdcs"]-ds["rsucs"]).rename("radflx")
compute_dTdt_save2zarr(flx, conv, ds, varout="dTdts-cs-from-flx")
del flx

flx = (ds["rldcs"]-ds["rlucs"]).rename("radflx")
compute_dTdt_save2zarr(flx, conv, ds, varout="dTdtl-cs-from-flx")
del flx
