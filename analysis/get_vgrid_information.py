# Obtain vertical grid information and store in separate netcdf file for later use

import numpy as np
import xarray as xr

path = "/users/staff/avoigt/cmip6-crh/analysis/vgrid_information/"

def vgrid_halflevels(ds):
    """ Stores halflevel information for later use. 
    
    Note: requires python >=3.10 because of the match construct. 
    
    Further notes:
     - the HadGEM, UKESM and KACE models use height levels, this requires special treatment and is not covered here """
    
    mod = ds.attrs["source_id"]   # model name
    var = ds.attrs["variable_id"] # variable name
    
    if not var in ["rsd", "rsu", "rld", "rlu", "rsdcs", "rsucs", "rldcs", "rlucs"]:
        print("WARNING: vgrid_halflevels requires radiative fluxes inside the atmosphere!")
        print("WARNING: Because these are not in the input data set, the function will return no vgrid information!")
        return
   
    # for some models it is necessary to remove time axis
    def remove_time(ds):
        if "time" in ds.dims:
            ds=ds.isel(time=0).squeeze()
        return ds
    
    if "HadGEM" in mod or "UKESM" in mod or "KACE" in mod:
        print("vgrid_halflevels: model", mod, "is height based and needs special treatment")
        raise exception
   
    match mod:
        case "AWI-ESM-1-1-LR":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_halflevels_"+mod+".nc")
        case "BCC-CSM2-MR":
            remove_time(ds[["a", "p0", "b"]]).to_netcdf(path+"vgrid_halflevels_"+mod+".nc")
        case "CNRM-CM6-1":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_halflevels_"+mod+".nc")
        case "CNRM-ESM2-1":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_halflevels_"+mod+".nc")
        case "INM-CM4-8":
            remove_time(ds[["ptop", "lev"]]).to_netcdf(path+"vgrid_halflevels_"+mod+".nc")
        case "INM-CM5-0":
            remove_time(ds[["ptop", "lev"]]).to_netcdf(path+"vgrid_halflevels_"+mod+".nc")
        case "IPSL-CM5A2-INCA":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_halflevels_"+mod+".nc")
        case "IPSL-CM6A-LR":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_halflevels_"+mod+".nc")
        case "IPSL-CM6A-LR-INCA":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_halflevels_"+mod+".nc")
        case "MIROC-ES2H":
            remove_time(ds[["a", "p0", "b"]]).to_netcdf(path+"vgrid_halflevels_"+mod+".nc")
        case "MIROC-ES2L":
            remove_time(ds[["a", "p0", "b"]]).to_netcdf(path+"vgrid_halflevels_"+mod+".nc")
        case "MIROC6":
            remove_time(ds[["a", "p0", "b"]]).to_netcdf(path+"vgrid_halflevels_"+mod+".nc")
        case "MPI-ESM-1-2-HAM":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_halflevels_"+mod+".nc")
        case "MPI-ESM1-2-HR":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_halflevels_"+mod+".nc")
        case "MPI-ESM1-2-LR":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_halflevels_"+mod+".nc")            
        case "MRI-ESM2-0":
            remove_time(ds[["a", "p0", "b"]]).to_netcdf(path+"vgrid_halflevels_"+mod+".nc")
        case "GFDL-CM4":
            ds = remove_time(ds)
            ap = np.zeros(34) + np.nan
            b = np.zeros(34) + np.nan
            ap[0:33] = ds["ap_bnds"].values[0:33,0]
            ap[33] = ds["ap_bnds"].values[32,1] 
            b[0:33] = ds["b_bnds" ].values[0:33,0]
            b [33] = ds["b_bnds"] .values[32,1]
            ds_vgrid = xr.Dataset()
            ds_vgrid.attrs = ds.attrs
            ds_vgrid["ap"] = xr.DataArray(ap, name="ap", dims=("lev"), coords={"lev": np.arange(0,34)})
            ds_vgrid["b" ] = xr.DataArray(b , name="b" , dims=("lev"), coords={"lev": np.arange(0,34)})
            ds_vgrid.to_netcdf(path+"vgrid_halflevels_"+mod+".nc")
        case other:
            print("vgrid_fulllevels, model not known", mod)
            raise exception
            

def vgrid_fulllevels(ds):
    """ Stores fulllevel information for later use. Sister function to vgrid_halflevels.
    
    Note: requires python >=3.10 because of the match construct. 
    
    Further notes:
     - the HadGEM, UKESM and KACE models use height levels, this requires special 
       treatment and is not covered here """
    
    mod = ds.attrs["source_id"]   # model name
    var = ds.attrs["variable_id"] # variable name
    
    if not var in ["tntr", "tntrs", "tntrl", "tntrscs", "tntrlcs"]:
        print("WARNING: vgrid_fulllevels requires radiative heating rates inside the atmosphere!")
        print("WARNING: Because these are not in the input data set,",
              "the function will return no vgrid information!")
        return
   
    # for some models it is necessary to remove time axis
    def remove_time(ds):
        if "time" in ds.dims:
            ds=ds.isel(time=0).squeeze()
        return ds
   
    if "HadGEM" in mod or "UKESM" in mod or "KACE" in mod:
        print("vgrid_fulllevels: model", mod, "is height based and needs special treatment")
        raise exception

    match mod:
        case "AWI-ESM-1-1-LR":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "BCC-CSM2-MR":
            remove_time(ds[["a", "p0", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "BCC-ESM1":
            remove_time(ds[["a", "p0", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "BCC-ESM1":
            remove_time(ds[["a", "p0", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "CESM2":
            remove_time(ds[["a", "p0", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "CESM2-FV2":
            remove_time(ds[["a", "p0", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "CESM2-WACCM":
            remove_time(ds[["a", "p0", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "CESM2-WACCM-FV2":
            remove_time(ds[["a", "p0", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "CNRM-CM6-1":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "CNRM-ESM2-1":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "GFDL-AM4":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "GFDL-CM4":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "INM-CM4-8":
            remove_time(ds[["ptop", "lev"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "INM-CM5-0":
            remove_time(ds[["ptop", "lev"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "IPSL-CM5A2-INCA":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "IPSL-CM6A-LR":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "IPSL-CM6A-LR-INCA":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "MIROC-ES2H":
            remove_time(ds[["a", "p0", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "MIROC-ES2L":
            remove_time(ds[["a", "p0", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "MIROC6":
            remove_time(ds[["a", "p0", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "MPI-ESM-1-2-HAM":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "MPI-ESM1-2-HR":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case "MPI-ESM1-2-LR":
            remove_time(ds[["ap", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")            
        case "MRI-ESM2-0":
            remove_time(ds[["a", "p0", "b"]]).to_netcdf(path+"vgrid_fulllevels_"+mod+".nc")
        case other:
            print("vgrid_fulllevels, model not known", mod)
            raise exception